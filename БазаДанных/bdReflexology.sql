PGDMP                      |            Reflexology    16.2    16.2 :    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16477    Reflexology    DATABASE     �   CREATE DATABASE "Reflexology" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Russian_Russia.1251';
    DROP DATABASE "Reflexology";
                postgres    false            �            1259    16480 	   meridians    TABLE     �  CREATE TABLE public.meridians (
    id integer NOT NULL,
    name_m character varying(255),
    number character varying(255),
    info character varying(255),
    img character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    standart character varying,
    vse character varying,
    info2 character varying,
    info3 character varying
);
    DROP TABLE public.meridians;
       public         heap    postgres    false            �            1259    16479    meridians_id_seq    SEQUENCE     �   CREATE SEQUENCE public.meridians_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.meridians_id_seq;
       public          postgres    false    216            �           0    0    meridians_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.meridians_id_seq OWNED BY public.meridians.id;
          public          postgres    false    215            �            1259    16511    methods    TABLE     �   CREATE TABLE public.methods (
    id integer NOT NULL,
    name_m character varying(255),
    info character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    info2 text
);
    DROP TABLE public.methods;
       public         heap    postgres    false            �            1259    16510    methods_id_seq    SEQUENCE     �   CREATE SEQUENCE public.methods_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.methods_id_seq;
       public          postgres    false    220            �           0    0    methods_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.methods_id_seq OWNED BY public.methods.id;
          public          postgres    false    219            �            1259    16493    points    TABLE     �  CREATE TABLE public.points (
    id integer NOT NULL,
    name_p character varying(255),
    number character varying(255),
    info character varying(255),
    img character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "meridianId" integer,
    "xCoord" integer,
    "yCoord" integer,
    location text,
    recom text
);
    DROP TABLE public.points;
       public         heap    postgres    false            �            1259    16492    points_id_seq    SEQUENCE     �   CREATE SEQUENCE public.points_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.points_id_seq;
       public          postgres    false    218            �           0    0    points_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.points_id_seq OWNED BY public.points.id;
          public          postgres    false    217            �            1259    16636    ratings    TABLE       CREATE TABLE public.ratings (
    id integer NOT NULL,
    rating_s integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "symptomId" integer,
    "methodId" integer,
    age integer,
    gender integer
);
    DROP TABLE public.ratings;
       public         heap    postgres    false            �            1259    16635    ratings_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ratings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.ratings_id_seq;
       public          postgres    false    226            �           0    0    ratings_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.ratings_id_seq OWNED BY public.ratings.id;
          public          postgres    false    225            �            1259    16538    symptom_points    TABLE     �   CREATE TABLE public.symptom_points (
    id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "symptomId" integer,
    "pointId" integer
);
 "   DROP TABLE public.symptom_points;
       public         heap    postgres    false            �            1259    16537    symptom_points_id_seq    SEQUENCE     �   CREATE SEQUENCE public.symptom_points_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.symptom_points_id_seq;
       public          postgres    false    224            �           0    0    symptom_points_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.symptom_points_id_seq OWNED BY public.symptom_points.id;
          public          postgres    false    223            �            1259    16522    symptoms    TABLE       CREATE TABLE public.symptoms (
    id integer NOT NULL,
    name_s character varying(255),
    info character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "methodId" integer,
    pointss text
);
    DROP TABLE public.symptoms;
       public         heap    postgres    false            �            1259    16521    symptoms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.symptoms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.symptoms_id_seq;
       public          postgres    false    222            �           0    0    symptoms_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.symptoms_id_seq OWNED BY public.symptoms.id;
          public          postgres    false    221            3           2604    16483    meridians id    DEFAULT     l   ALTER TABLE ONLY public.meridians ALTER COLUMN id SET DEFAULT nextval('public.meridians_id_seq'::regclass);
 ;   ALTER TABLE public.meridians ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    216    216            5           2604    16514 
   methods id    DEFAULT     h   ALTER TABLE ONLY public.methods ALTER COLUMN id SET DEFAULT nextval('public.methods_id_seq'::regclass);
 9   ALTER TABLE public.methods ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    220    220            4           2604    16496 	   points id    DEFAULT     f   ALTER TABLE ONLY public.points ALTER COLUMN id SET DEFAULT nextval('public.points_id_seq'::regclass);
 8   ALTER TABLE public.points ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    217    218            8           2604    16639 
   ratings id    DEFAULT     h   ALTER TABLE ONLY public.ratings ALTER COLUMN id SET DEFAULT nextval('public.ratings_id_seq'::regclass);
 9   ALTER TABLE public.ratings ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    226    226            7           2604    16541    symptom_points id    DEFAULT     v   ALTER TABLE ONLY public.symptom_points ALTER COLUMN id SET DEFAULT nextval('public.symptom_points_id_seq'::regclass);
 @   ALTER TABLE public.symptom_points ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    223    224            6           2604    16525    symptoms id    DEFAULT     j   ALTER TABLE ONLY public.symptoms ALTER COLUMN id SET DEFAULT nextval('public.symptoms_id_seq'::regclass);
 :   ALTER TABLE public.symptoms ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    221    222            �          0    16480 	   meridians 
   TABLE DATA           y   COPY public.meridians (id, name_m, number, info, img, "createdAt", "updatedAt", standart, vse, info2, info3) FROM stdin;
    public          postgres    false    216   7F       �          0    16511    methods 
   TABLE DATA           T   COPY public.methods (id, name_m, info, "createdAt", "updatedAt", info2) FROM stdin;
    public          postgres    false    220   Rr       �          0    16493    points 
   TABLE DATA           �   COPY public.points (id, name_p, number, info, img, "createdAt", "updatedAt", "meridianId", "xCoord", "yCoord", location, recom) FROM stdin;
    public          postgres    false    218   �z       �          0    16636    ratings 
   TABLE DATA           o   COPY public.ratings (id, rating_s, "createdAt", "updatedAt", "symptomId", "methodId", age, gender) FROM stdin;
    public          postgres    false    226   .�       �          0    16538    symptom_points 
   TABLE DATA           ^   COPY public.symptom_points (id, "createdAt", "updatedAt", "symptomId", "pointId") FROM stdin;
    public          postgres    false    224   ћ       �          0    16522    symptoms 
   TABLE DATA           c   COPY public.symptoms (id, name_s, info, "createdAt", "updatedAt", "methodId", pointss) FROM stdin;
    public          postgres    false    222   �       �           0    0    meridians_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.meridians_id_seq', 20, true);
          public          postgres    false    215                        0    0    methods_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.methods_id_seq', 13, true);
          public          postgres    false    219                       0    0    points_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.points_id_seq', 96, true);
          public          postgres    false    217                       0    0    ratings_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.ratings_id_seq', 21, true);
          public          postgres    false    225                       0    0    symptom_points_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.symptom_points_id_seq', 1, false);
          public          postgres    false    223                       0    0    symptoms_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.symptoms_id_seq', 32, true);
          public          postgres    false    221            :           2606    16489    meridians meridians_name_m_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.meridians
    ADD CONSTRAINT meridians_name_m_key UNIQUE (name_m);
 H   ALTER TABLE ONLY public.meridians DROP CONSTRAINT meridians_name_m_key;
       public            postgres    false    216            <           2606    16491    meridians meridians_number_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.meridians
    ADD CONSTRAINT meridians_number_key UNIQUE (number);
 H   ALTER TABLE ONLY public.meridians DROP CONSTRAINT meridians_number_key;
       public            postgres    false    216            >           2606    16487    meridians meridians_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.meridians
    ADD CONSTRAINT meridians_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.meridians DROP CONSTRAINT meridians_pkey;
       public            postgres    false    216            F           2606    16520    methods methods_name_m_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.methods
    ADD CONSTRAINT methods_name_m_key UNIQUE (name_m);
 D   ALTER TABLE ONLY public.methods DROP CONSTRAINT methods_name_m_key;
       public            postgres    false    220            H           2606    16518    methods methods_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.methods
    ADD CONSTRAINT methods_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.methods DROP CONSTRAINT methods_pkey;
       public            postgres    false    220            @           2606    16502    points points_name_p_key 
   CONSTRAINT     U   ALTER TABLE ONLY public.points
    ADD CONSTRAINT points_name_p_key UNIQUE (name_p);
 B   ALTER TABLE ONLY public.points DROP CONSTRAINT points_name_p_key;
       public            postgres    false    218            B           2606    16504    points points_number_key 
   CONSTRAINT     U   ALTER TABLE ONLY public.points
    ADD CONSTRAINT points_number_key UNIQUE (number);
 B   ALTER TABLE ONLY public.points DROP CONSTRAINT points_number_key;
       public            postgres    false    218            D           2606    16500    points points_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.points
    ADD CONSTRAINT points_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.points DROP CONSTRAINT points_pkey;
       public            postgres    false    218            R           2606    16641    ratings ratings_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.ratings DROP CONSTRAINT ratings_pkey;
       public            postgres    false    226            N           2606    16543 "   symptom_points symptom_points_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.symptom_points
    ADD CONSTRAINT symptom_points_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.symptom_points DROP CONSTRAINT symptom_points_pkey;
       public            postgres    false    224            P           2606    16545 3   symptom_points symptom_points_symptomId_pointId_key 
   CONSTRAINT     �   ALTER TABLE ONLY public.symptom_points
    ADD CONSTRAINT "symptom_points_symptomId_pointId_key" UNIQUE ("symptomId", "pointId");
 _   ALTER TABLE ONLY public.symptom_points DROP CONSTRAINT "symptom_points_symptomId_pointId_key";
       public            postgres    false    224    224            J           2606    16531    symptoms symptoms_name_s_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.symptoms
    ADD CONSTRAINT symptoms_name_s_key UNIQUE (name_s);
 F   ALTER TABLE ONLY public.symptoms DROP CONSTRAINT symptoms_name_s_key;
       public            postgres    false    222            L           2606    16529    symptoms symptoms_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.symptoms
    ADD CONSTRAINT symptoms_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.symptoms DROP CONSTRAINT symptoms_pkey;
       public            postgres    false    222            S           2606    16505    points points_meridianId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.points
    ADD CONSTRAINT "points_meridianId_fkey" FOREIGN KEY ("meridianId") REFERENCES public.meridians(id) ON UPDATE CASCADE ON DELETE SET NULL;
 I   ALTER TABLE ONLY public.points DROP CONSTRAINT "points_meridianId_fkey";
       public          postgres    false    218    4670    216            W           2606    16642    ratings ratings_symptomId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT "ratings_symptomId_fkey" FOREIGN KEY ("symptomId") REFERENCES public.symptoms(id) ON UPDATE CASCADE ON DELETE SET NULL;
 J   ALTER TABLE ONLY public.ratings DROP CONSTRAINT "ratings_symptomId_fkey";
       public          postgres    false    222    4684    226            U           2606    16551 *   symptom_points symptom_points_pointId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.symptom_points
    ADD CONSTRAINT "symptom_points_pointId_fkey" FOREIGN KEY ("pointId") REFERENCES public.points(id) ON UPDATE CASCADE ON DELETE CASCADE;
 V   ALTER TABLE ONLY public.symptom_points DROP CONSTRAINT "symptom_points_pointId_fkey";
       public          postgres    false    4676    218    224            V           2606    16546 ,   symptom_points symptom_points_symptomId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.symptom_points
    ADD CONSTRAINT "symptom_points_symptomId_fkey" FOREIGN KEY ("symptomId") REFERENCES public.symptoms(id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.symptom_points DROP CONSTRAINT "symptom_points_symptomId_fkey";
       public          postgres    false    222    224    4684            T           2606    16532    symptoms symptoms_methodId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.symptoms
    ADD CONSTRAINT "symptoms_methodId_fkey" FOREIGN KEY ("methodId") REFERENCES public.methods(id) ON UPDATE CASCADE ON DELETE SET NULL;
 K   ALTER TABLE ONLY public.symptoms DROP CONSTRAINT "symptoms_methodId_fkey";
       public          postgres    false    4680    222    220            �      x��}Io$Kr��+�0�W�"cɭ��#M�i ���B�ȁ���� �ɪW�M-�H�����HfU2I&�2��~ɸ}f��Gd$�5�G����f�-�uwV����_��k��tu���u�\ݮO�g��7�j������|���u�_���:ɟ�=�Y�^���W������5U_�_ݯ߭f����U���.�nU�E�zc�w����r�J�~@���)~��?S-�_.֧�ͅ�5W��Vm/��'귪Uթ:�J��S��j2���������z;Y�Qj���r�Fu�:J�wT��/U��R��+���S;ޙ�ɒ�ׯT���<Y����Ϸ�zwu��X*=�$��Z}}�.��{��xg4F��a:ui����h0��Ó����dX��������w�2�i^&y���{�g�����Eӣ
�Ԯi�o����o���I�m,,�U}s��Wc�?O�w�����d�j���gU�FF��j�J�9�����^Rm��ϰ��c�묶�(-�Su�������j�S�������?���;3��Kz�>�v*�n�f�V�N��~'/��������A<�T|��/y��w�W�d�q/Q��d�(���
��K�Ӗ�0s��L�ـ+�FNͻ�P�U�m���Mh*y����gP�]"ӵ��o����	�nE53U)��
kuI�T�j���i�*Y`L���;<$���ӄ&�6*Q�T�˒:�q�]Q_*z 	��u0������O�Dh�!�X_�%�/�/�OflM]�Lh�:�@��&��������͘ƥ&gj�G�8/��j����0E�K�oi)�)��)W���xJ<�Gj��Q�����Sb��@�Kመ�Q'M�t��֙�AT\�jqʋAtC��v�Q5��`PW#�ˬ$����G>M��8���
���q�1�ЩMZ��Ħ�c>�w����`�UrB�t	��P�/�������>���(���-p��,3gd�峈w��ʻ�>Bϼ�N�<vw����^�ȝ|Q�:���*�Pu���]����`��o��U&\$�<u�C��]�w���5����ܩO�4{j��j�s*��~�*��B���T\P�)�}�Go�RZ@*�Q�Vd5��>-���O�*�����T8D3�*0�l/������yjz&�g؏돫����0��j.�Գ��©��|OcR�9O�W�"Fe�R�T�������o���n�.т��f�kj�������Լ���x��/!�L���y��t>e	�d,��9x�L��K�F!8j��r�Z�C��iIJ�~Cx۝8ڍ"����qbE��k0���ܺI<��c!Eޣ^���	�H�+�V:Ҝ,puC_���]����CG���v�I������k�¸'��b��`��u\��_ �h�i���]$���;�������MB>1o0}В+RۚA8�a��K�2U��G�k��gyLq���=t�}R���ғ�տ�����n�P�1� �O��{�ҳ-�]�RV6.;3�üV$`������'&�%YW��:�����;r._F�%�)��-�`f�Gp�t�Q/�d�C��/������TZ�`����<*�G/�'��������x���{�E�k1�G�t�����z�ژ$��
�%i�����,OG���|��bx��jR��z$�Ҙ͉�3�"i�Q���Z���Qxj��Lk��k
�kz�^�$j�'	&�����d	>%-�����Ygk��R.��.mP�4�!	"������w�ŵ���?�Z4��@��5h#W�[�|̩�3��i��G�����L+L�XS��bo�,� ����2|�S�
���SAԟL�2�[��9m��f��m����e���4 ��=t8�~O��B�nv�H���yv��S���6�HV�I�H���a3^�6ū~'Z�P�,B+�Ī�%�ӶY5�n.�u��!k��Ћs.����x 4���q<w8"cq�+oҩhFfL�В�8`g�����pF��Y�%*�NÒ)؜Lz�Ь�8lI�}�p����o��+�P�ޣ��،z!�;ҹ��GO�����$�B���ͼ��T��_�(�R�@�l���x^Au3sEmҰf,�n��8K���_�t�ӡ��3v� ,W�w��,nJu4�pr!�m}Q��L�^7��
����l��e���ͼnÇ~��2�ڳ���f��j�ܢ,�̹:�@�Q���} �ų=M |.h�o��7C���b���\�FV!	�h����*b��N�Oz�+����Ϡ
�}R)��&��z_����TM��P�-1ڴ�+��7�AU8�7̉(`*M�������� �A��� �Ġ-�B���i8@\�c�P��������X4�
y(	�!WKb�`9?���$��=�d� @\���{�^�Lԓ�t��}
�GS�"�Ǯ�������U��<v𴷺O�Bƺ�8�S���w�%Ǜ��8�R��q��lN�{�c��)\�:��U���W/8�g^����K*Wz(�8ޱ�8@0:�,������ڀj�Z�	B����z�B�&�x��bϲa��:���[4{º�XQ.!��/������ߟ�Q���u�o�
h/��Ȗ�o�'���3�K����|T�;6�ʴ]�x���'��}1�9<<w{iv�����ãtx�+�A�p4���`A	���"�-G�*J�=��:���0W�w�%c#���l�h ���(C�uT�O�1���y��$�~X"�V�ݵHks)$�:{�A(\���%ΠjnV�G�Z�V�����S�T�T�e�SPG����=/�0n������ ���9s�UW.��lY���C��6(��촵o�j�.x}��s7!uq�k!�6���\���0��(bC�h"E��A
��<�E��KKlwꞓ� ���9l��Ĺ+��5ȎQ������J&�l+N^PwO��R�R��~W�!�Ό㹽�����6�\�R���]��?�b�l�w4CK�-i���49W�e�{� ��y!��ʓ$�$�yE	gQ��Մ�.�!�&H��J�u��[����e�O8q޵׶jU(g&B
������3ԠSti�1e�0�0!$<����3�6���~��Bǚ��&�>OP�VOL1Z��?-bI�P7�Á7��
D^z~�2��ܒRA�'��d5s��b��2�ʧX�٘��g'�_���.:�'�����u\nu��x\��s�τ,J��7�!�I6���@�G���\��	2֩��r2Y+� �au�5�[���Ө-��]�3����Ⱦ��;0��'�A���_eOTN�q�����o@�;|��^�\�k;,���C��b�li���J��c	��� P(��Z��
�L��v�R���3i>�=&5�f�X�b{���N�d�d3j��V'��C��5
�8)�k<V1�ӎ�e��1n��d���k���e�Q�M�l9f��5�[�=�?$�{ �Z�����Z����W�l��9�Jj� <������4���I�K��q2�TO�'t2�6�R�*^�آAK�������*=��7:������K���(�<z���'��q�pxpӣ�,���nQգ�Gqk�� >R^=��J�g� ���A��9�����·�����V%�7�+�f ����(꼢�{��D�#�&�fj|s��6o0��X��gz��I`bG/�#����.��j|�[�Ztn�j�/ւ�5bj���LTlǢ���\M�o�R��˫esl�[쑥+Ru�Q�Y Ƈ�6ֱ ���D2k�1f�i�a���:�5�*���6$U��.t�Ϩ"��V��'��)�"o���}��6[��UJ+�)������:�:6T� ay�L�)��`���UĶІ>��Z�ѶG��@v�f'����m��5��D�E �h����{{�j�F���GjT!OmP9l������
ޤ���~85��?9�"uV�Z�_�jb�PR�I皉׊�G8�$M��Hi�:�ľ�@��ָ�:��1    ����^�w �h~Xj}�I�����iG�_D�̴��;��n�؀�����L~�I~���	�l�Y`��5�L��N���R���`Šd��x��oh��N|НjE�n.7y�j��Cb�;�f�a�+�Ҳ2L˲,����(=�f�n�W��{G1���E��yqnuU׬iĭ$oB����}o�Z	2���E}\#/A�l'#�}2�;/D>�d�K��G8�f����PK�r�(&�n��7ܭ"^��O�;�:�H	K6���$��B�)8A�F�a� r>����X�42g����&���#�\+m� @uW�k|�c5�y D�1�&�l1�Z��`����&<��$�>�;�V�@�j��lְ.�5����z�[�ku�9��Y�r�E{���@�TQ�� ��{�L9��^Qݮ���O��I\݂(�9��*��'m�D�H��C<5�Sw�V"�Uc�^P��N6�D��0ɮM�-M~|��ݖ�.��ρ��Igjb�H�a��)��2ɩ�3��`��S�;�aN(4�a��.ʥu�n�yF��K�E������HfKx��?Y��F�.Ut	;
q���2�o�ژ�٫��P�_n���*q�"���7������)����/��wg^���z�v6�,�������:�q�~������#Qm㿍Ga��M*.ۆj�����dc����d+1!��IY�sN�.�f�>���vv�N�ٱBG�.D��5[�b��ڊMX�FI6��e/~��C�J�������&��Y�C��(��糌{>�E_���)k�������=�G[��{�Sݱ� ta�qޮ���0��$����;)�0\�e�G#J����z����m��q�$�}��G�4�51?Յ�G��##�N�ƾL#��/��^�1'r�!�8�M
��;s���Eb��6%��򒒊�8~T��GN��Z0V�w���B2]��/>\���M����h�-&���0Ѫ�j��zD��duUF�R+�A0KA3�������g+�]�=�а�x�WS��"���2 �$à�rɭ�����4طAM�J3tU�AU�d�+eft�J
Ӵy�F�ӈৢ^��z�:N��HHr�g��:��9��Qs��%��M��`F��XH!�#ycpΤ<sFH8qR��&|#)�}{F_TW�ҋa0�����p�^~���"��5eD���������'\�O»B�)��L����?=�yr�w�q�)�}�"^3e�ݓO����)+��o�[H"�!�x��D �&a�Q�f�}:�ZZ��㓎[sBq�F,Cö*�hg?��\�b9K�/x�9b����tm�v��.�0�V�G0�Fggh;w�7|�#����O���+�'�ϲ<����|_��u~�3��>�|�o�ߑʤ��şw�?S���CWnc;-��Nj�8���� ��(ٱ ���Ok�H��&*���W��cߍ��u���
�P�w^f��a��ix|����(���4������˼[���Y������Cl=xT	9����j�<x:�r|��QN<�sOyD���N�̩%��4�#r�K��=��O�YD�����Kq;��)VA�P%x�di ��`�4�W����{�T� ��-vS���DM_#�p�����،X�{���q��т?M:����6��Řå�y�Ao�9L�Xh���p-wdt�;�	�r�6_[ns]�H{����ɦ����w�E���⨸��E�:� �w������]1�$4Y���]i��-�Z���"��0�� '�k')R�T����WO^Fr�-Pu����I]�&&��=�6��hx�K�R��WA2�M M<� A���K3�,+�e���5A��W�^;ħ��gz�gRᬵ�]59F��\/�
��p���rf��l.eI�!ے�ĭ$L^ (��(a,��Bd�G0�5�,W�١��;��5���5�	�;%�s�An�E�ޕ$����Ĥ�4Z�֞T�-���r���e����dE)G��	�ѳ��g���t	6-�Q��c᜴Rhߴ���BI8t���ҭx��KwΙ*'��jx6�]��)9_��f�t��<3+�����di����>`�Ф��׷NaD����TH�t
H'Nk�dՕD���{��c@���������g�0C�)�}H��=޴�ԤaXIf/��@��ٗX�>�MZ�K-�ڠ���P��,��V�׎t�N���?��ϛ!��ܜ�vVs͇�5|eW�N�|8���&�l�n�O�\ñ�񃖼D2D*-���A:;�q���=H_)���az���ң~~�g�'��Q��}Q�v�n/�7�}T]	w� �?�N�f��"���.Y�7ӊ%�a�S��^%�$}榠�&Q��h�@`s�r�����I%��$�t<���#���XSkc�C�W��p��2���B�sC�iy/`�{Z�H�>�!����Q��a��f�Ї�M��5�G7*\�r��z������2�y�ٳp�������h�H$�[��E}nP���s�eb>\�,y�&;��p2v2hA���_R��?y��yӵ���P��
�I/a���ľ�<�B�Og������Y�MJ2O�Ml8��^���И}�!0I�'7 ��)��2�䑜���8l��MHs��c֑P�\��b�$޺��������猖9w�RK�F���'����1�s�r���`\��ǘk�P2����4o2�#�8e���b�8;G$���k��c�yP��pQ�G�3fz_$���X΢�E��`�DAsB��� �F6O	~�e�1#�%�.�=���N��QɎ�.�b���R�Ƀ�{\�iKX]V<%A�ע�Ly��8��ތ��Օ��$8����d�̖��]Q�ܢ����\���n6���`����,-�� =8�L�Y�������b7�(G/��ݢ�����bR�L]2�߬���a��ԶZ�74�Պ�[��"6G�ݛ��_�Yy_�=�QM��D�MUO�ewv�k2o\�P7�T����Կ��r���_D/D���x�	�.��=�ao�dt��~�D�<�αAP5��&�1�?$�x���3����	��I�����6�gL�N�����F`G3��(��yM�IQ-�@��9*�n8'�P�.�yK"0�����ҥB{��󔽵(�ҿ�N�*L����������ҋ��K$���ӍN�N�m߶��I�_J'����HU�ݞ<�}��$.�P���h�il3�o�n����Og��p�����-z��[+�G�ڪ�ŧ�ܶP����z�����^�%/p�wROSě�x�f_t�lG�Mq���)5�mX��M"~��Q�ڽI�I���V�S���{�����jSOV���E���@�j.����ԯ]e������^|S0�A��A2���3|���K���K�Ѵ��\"�=�cǹC �=���7"��r/�QK�0����,�U��U��ɘ����$*��=�a�5W�Qw?�tk.�����Y��KqD�����B�<�3@ζ7���Ί�bc*��7��<�m��2�#����A}'�v��;�;��M��i�G�J�}���>��I*Bf��N�#D�M �>+��}��{��bv?��'LE����:J>0˪��J��2���^��L'Q�PPH�T���^�P��9�4��~�ȥ(�I���B~��,�Uo�,��ہ��q�/�f������wD�\���J�9�B��k��B��Ӈ!���ư!�y�1q��Xx�r�7&���wڧs	r�ؠ*�i��җ(��94Dj�����r�/B;�ؽ������/wz�q���ֶ� ����C�B�@�ѭ�<6���M�D���� �N�$��w��`�ݸ�5H�gTS�Φ+0��VLV���4�MyC11"Jzn	�k9/ƉD;�Eu0�Z��0��e��%��}���{.V��؝��^'��e'���w�I~�g���o��pV�kn������S��V����q��=�{�[����� -�|��Y9H�Gã��w{   �����$	�(/���Wy�G��2�s��)��p��8�tSH|���&�Г��H��#=A6f �m�m1G[����1��i�h%M�ʽ�b�]v��VH3�m�i�ͺ�g�y~*��;�+���7q/�+��ӓf�냵��	A*$�^3�hf��ҍ-��'q����[}�tRM���#�M�G���[	8M�)����01�E��>���	1@؜]��Vۦ|��my�h��&&B���[q���
4��/��{�=t��5 (�����YȞ��ǎo�y\��f��{�L����x��{v�t�>ɡ7k}m��G�A�	���ʦV���M�ci��s����XL�ai�=('^���!�f�#������Vty�_&��P3��w;�y�����l �LLN���%^�N��F�y���Y1��#:~O�ߨ���^�Kж�y�낋�T7`�	�BB.��l���À�'b��h0��G`�XM£�����]]�}���5��W���StŞ6���֑�ɎQo��T���;b~�b%\ҝ:��$�����4�n��l�n��W^FUh�����*K& g�������+�zFZ�?%������u`XnwU���rx�+O^*��,��`t����Iz�u^e�S���EY�f�~U��E�ύ��8\�m���{	���e@�^4q�Y����E,^\䋧U�ꖚ�4��y;ѽ���.B`%���Յ\z��i���)�m]�
G�}p̑+v��}�-Y����r̀�v�zCW�ȗ�l�u��j���q�� :h�N(�n��ա7]� �G ��.�az��`41��\�.Tvm�����T[�'5Ξvb
���<&��vGˤ�D�uk��Mt�11�M�����G���lh�gg�p���m-"����4�l�_k�0N�Q�cn0��xG�H��x��u������0�Oz�K��"n��e ����wH�0���s�j^ݢNr�^U�~\����5���>�-�$�xl�J:p}}��"B�?7h�Э��Ꟍ���ܝ>gw��+ב�,ߌ~�KƉ� Ħ�k�F� )���̓��a��li�AR��I�-��i��O����a:u�����ht<8t���[	�ы���K��G�Iơ�R�}z�Ӆ�ְq<ZZ�mb_w�u�- ����H��ӤI��|�?\�eq�0�2.�=��؝��ʫɣ�-\2ÛK����k�¢<�K�	�>"y�t��L���2oW%N0Eo�N����\w�yȼ�k����3a,��݉M�'����4ǜ�&I­��I!��s����S'C�9k�I���T�|B��A�5	�¢|��'H���wη�(�baI���p� 5�%�O�j�/�t&�f�Z�k�f9~��+�n|	��Ӫ����!I�s�D�27� }����7�|i���	6��&����k�{G�������B��N���Y��	ɋ	;����|sV�n��;F� M�B>6]��+Dpbn#D|�m `��$粵�qwI'ԛ��$/.�叼]�ڛ�$�1N��[Q��̉E�Ҽr�'잶�!�J_��a��8籔5ybu�6�����\��$���=o���A����N�]ڟh�Am`������
��JO��6�=u�ș�<�3*�*3��K�tdj� �D!6J��Y�\�vϝ��6s$y⻌ڤ����O�9ޗC���t���4km�W�
�'�ߛ�˂�T�v���tj3~E� ������\pNΔI��dM��I1�o�S�~�K�ڢ'�*�짯u}e.����������L�*.��=�b^ȹt��*�r����є<��I�#]#�E'=o,췝�2S_�Y�˝3�sc�
���z�{Z|2����|���yKc3��Co�>�I��W�,�zud���yX=#�&�X��ރ$�ҥ�7�+}]|�м#���t��iOj2���}�؅�yyP=R���<
��wA�+�p�q樴��z�n�{7�+��I���҇��w� ڿ�?�QF� �����w�[Ut1"����X>YHT��(gcWHV��t-�k��qO�+GZk0zګ2ɣz���]�'�������oJk�c_�_�Ipө��V�Lɦ[�uĒ�c
gZ^a�o�e��E�"�1���]!_E��d�޸���5��|`J�j��FY~�2�2>�1/⚚<$
����f��4����\k���]�97�Y܅���y��X����b��cАv�^ot|�;J�y�O�������x�f'��w|xtr�����2�ͻ����*-i����������׼?�7\�f�!!��S�f�����A�q%�O�XqS�&�۳p /�K�-q�rn����o7�{��d{���2ۚ�H�H&����n�����ҁ��'��Hɻ�����0�0eXD��]{	$��|�e�ќ�5~�um����=�:�����N_@pޯ�k:�d�"p��ГHޡ>�qk�m��}�U�|�}#����TG�I�`���l��0#��|���lb�4ç�~X��05m��\m�u�����1���'�ŧ���hN��J`6$V�w06�gݍg!�:/�)G�����N�s��v�������4wP�IC�^$%��\'XŢ��6�e}��wѮ� i���Z;�7�+�:Wb&�ɷFTYAS���@�<��¤B����P�MiM�9��p�vC�GǄh��lQ�2�����-b sGz�p	�/��D%��F��c�_�*�g���2@2q�������-�v�e�u +�-©��Oo��fd�Pk���ٮ���@@�r��o8`��� �SI������G`C䳞����q�v�,��sS�s���.bг�7Nh�w���N���ןؘ��a]��N�$��!�y/x_gZ���rn����	��1)t�HOO%q4AU�q�1F^4�]owZ���i����7N�<v<�+_�휊k���������o_dA8Ը�7����nw����洚��f��,^C�q�����Ϟ=���y�C      �   `  x��ZKnG]����a8ÏDj�d��ϐ3P�mِ!���cCN�Έk��z�������g8��m�-���U�^UW��~��q[OuO�~����A|�Ǝ��K�':T2$>������'|n�1GO����~
=��QP�^��5�7+A�R)���w��}]���g�FG�
�v ��11D����wrG�8��z�C9���%�~�H��æ
�����}=�es��T5�/��Ҏ�谨��HKG�b��B�?TPD���v|&k�YZ�p������7�aĩ��Z<������/����ݗ#��Nd
!�(^T�־�_-�&J�1�6�Z��@�4��z����/���=
E�n�����㤗�!vx��;YAN?��C'�cR�۶FM��r��|n�+ga��|�)�7=L��I`��"~?�a���`9����7
a�W4C�3�(�3�=�״�^�%7X�s�h>0(8��}ja�08}�B����rgS��g��=M�����!>���D�=�ڤ�B$lޱ[��l_;�Æ��S N3,���7k���q%�ܮ5X�W�z�*~!��P�Y�|/X�*e�^v�^4І�D�H���pD�Bz��H����Bߤ�����kl��}+�3�'3lA�B�-� ��
���ah�!c�(rУ�H.H��[�&/%�ϧ[ h`��b���~�A�,͠^*Y�f�� �{*ƀS�<��8�T��S
�K�b����
��W���� ?X�6�d�?e�0@�Hy������(V�4�<]JhD�h�:h?�Hc	���O<�O��qO��%]����(��oY�k|]ѹ�<�ϰ�[�3�ɒ�~����P�9��XlA8'X�"4q��Z�o`b$����GG�ҙU]�*CeT��K�e
����[LL�1�Xz��k͠V�~6�f��>�&d:6|�8_Ϻ�5��,�N-DXk,G�UOԁ����r�Q��K�S;����#3j�b��3j�ٖ�^�O��P�Q�FZ�l�������-%���=�ؼk�E�9a]s�>� sN��e>����B�DD�N�;��� �l���Ι�OgA���� �7M�i10�3�F���򰫰�bp�L�E��sǑH��i��3�=I����q�n���SWtNĢT�򨫣�/����P|�gvs@X���)P
�����w�/��g��B��Nw��c�9�K]d�6�@�E�k��v�*T7��X��=r��W�A���/)�����F�$Z�5i������.�]㳥�n�����c֥�*��!�6�	6�w�q�Ɂ���L��9%/�ߒ�t̖�M��tƈ��\�R�-��~=� p�m��X疴'�݆���,�w3.(5[�zkQ�U�xZ�9�����J�rN�(յ��\�y�b]<)����_g��]],e/?%���r�{�;��?p�fWw���>�/^�lEy��NC�s��rg��c<弿G�r��8�yIu����X�z�5^`����O��)�2�y ^xRy��{����Q�-�����,*�;j����;U��tl�4��n��*�X*�#�Z�� T,Q}��Ju?X,����(������T��NC���}�`�<s.��<SlQFm�=�x�:ꕸhr�k��y�Q�Ki��K�v?��J����8n!ɠ5�ܐpz?�e�k��7��F��s�\����.R��_/�z�R-U��[|�kçƵ^/v��Լj
��y!�?%~o��"dr3������v�J}K�^8'p(�Uw�޸2����k��2��g�}�g��"�_Y��r���<�+�q����^�8�P���2g��@���&�5��E&��j�8�IR]�f�_,��L=�̌��4N�DN
i沸PZ�9]���T̗[Tpk� 'n���Ւj�B�0̞k	��+yv_~�Gz�Mg{�u��$���E�{�8�>�$OO���p(i�9�$sA.�ǩ���j6�X��N�%��ns��Q��M��+�hc�{��gYnlˈ ?>��And�[RWR��W\
�F/��XS���2lU�գR-{+��l����p�XbsP	���ˇӆ`��¯ ��Ty�.�
�<�˲��K����l�'�9]�I�P(��Բ:      �      x��]͎9r>������X��O��.>tЅ�L�a�GI��,F�s\x�^/|���R��n�Uo����?�Vόvaz���U���`��#X�9����?~h�W����4���_�/����������������M���D[�����cK����k������N�$T�7L�pw"��j�T\7�t��˟�d��f�
F��_�����{e䷛ã�����|�9<���G�Ї|�����f������O����?�g��K�������ؔ�����z�������>{��9۟=��*�=���7��;�9<���ǿ���^�/}�>��B�Qv��&R;
:֞K��[������gˬY�x1��QǛ;�6�D��d��O�5�>���A���K���bv��x��?�W.����j�7�O�-���t���u���H"�g�)i�i�t-�]`t�t�P�f��#p��OW��)��G4�G��������(��4 ��	��m���@�O�Tg���g�8��`>��'�"󧳵��<�᳻`*��������#�%-\�>`���<�O����O��]��e�A�S��9����7�oQ�����d!�f1U���^����<�o��� AO��E�
|O�I=MOu��tx��l~k�?���۟�.�si��J����p%T��J�KT�cx����>�j�Y.�&yvY�+)��7������w�k`�]�|������9]�4��W���Iţ�aJw��ZGZ�Q� �VĚ��J�B���N�@���������d�0�y��ޮ|�<�8����QE8�;gh�W\�'z����'�r�0���e.㦘�Gx�6O+X��n`&�Ͻ�����ւ֌+��Ɠ���?�342���y�"�[�4h"�L��7�.�1�֨�2��x9���n��/XӶ�����.	�wi��hTkM��f��ױt�&r�n;�]T��qs[ME����b�����B�5�~U$1Z0 P`OƉ�B���>�^-_y~"�	�p��GY15�D�Q����q�� O�r�*s����1��\�+�ؤ
�+Ŧ�F�#TTP�k@�BgmG\��������ȝ(��@��-��O�D�{	xM�_]�VY��� J��kn`�[�����ȣ�������gCL������4�'�r����d�|�#�$(:��u�/Z���W\w+EC �'\l��KE/����6LV�$X"|���'Б6�޿b�h�G`j���BL��&p9���S�Yoj�p��v5]����NW}�8mP'%<4���yՅ�p�L��A�rZ�:r :��^�P[G��e����CsW�y��	l�%^����<`(���Zk`]�� ����:!��y�V�T��bh
A�J����XZ��ԓ�F�i#�2���,�mM �we�&�th΍9���=/�5��X�%bT  �T�r$�A`�Zi��[�We�'�n�Y�C�ɒ��%��"�E#�b��k58[�4,��)X����=o ��AN`N@�ζ\�Ũ���MB��I"���� P�aY���X��{�N�L�EM�Q�9�C���e=9<�Yq�R�`9aζ��zC����:�x�2���@�<��d�B,��8O�cH���ab�q�Z�s�1��iZf5	�a��ƞ���L��qZ���u��	=��VNU�g/{�H+�)�A� z���R�(X�ܭŘ��P��4����i�p9����a��yH�4@E�HO�V��x�\M u���V��mD!zɶ�YA�� �EF-�� `� �� 1T�R��mn��W�����B��c�up��oe�HpT��Q�%��^�����Bd���������*����c^�����!O��S�ʃ�(x9���x�b
��U�8-���T2`y�ҟ�Qb�����od``����g�������9�I3T/EY-D1��N>�;��!(6�P�l5�.aw�M�X{�t'�����bh!�M���Ǭ𮑣4>��k颇u���8g�	*Z�J��W���ܺ^��|d�e��"{Y�R�#o��d5�)��_�?���&e2�$0}
��)��>`"3�T�`��i�J9�('DS2�����I�H)+��*��ó�p����s9���H���)Ƀ�r�݀�58��Q����G)[��^�h b ؀8b$����A���T�.)��-�+/�:��/�Zʃ�����t�z9�Zz}���d����:���h!��j5��P���� �2҈�~�;�"����qI���)V,�b���%q�p� ����a}N�����-g�,�R2�+���#����o�6�8 ����$��K��]I���U���4'݀/��ߎ�L���H
���UZ��?���i����s6:�"H@��:��Jb5�X��)T/}%g#�rgV�0��9a��j�����93&5EfB�k��� ^)X��AlR�cx�F�w���J���b��YQ5e�2�'FJ�Yx�/��e�P�H�Z�����}��P.jrH [�V�l14͏�f��HM5b]���� ��1�-�ZG8�ݙP�BJ4c�lE��Ђ~L�q���"�\���*nG�����*�v�y��W���b��kE��%)<e|��fC�I������y��1!��p��˃(a(���^�a��s@ �Xo宆f�0���X���"h8�vc%P�{��G �m`��6��W4�ȥ�+�Zh$y��y��d��[.5�ʤr���9b��LG��-�t@�ڬ �bh!�m�D �	�%Ȟ	I��3����ԝm)�nJn�:��K`̑Ѥ$��Ɩ�P�*X��c=��d� ��u]�T� \��y�Bu�5��Mi��gÆ�������nǄ���y�&ĕ���=a���u6	ge�|��t�c�=D�D^V��IGS�,�^2��15�Fm/m蜰�����R`n\��ӡ�$*׀���[zܧ�ZP%�qXܽY�2D�[՛>�X����:�"���Q�����ٔ$��"mO������zIL0��W�O��!�'ԁ#�R�M��I�z����&���k�&�	!A���A|��t��ϳ�2���<
E�� �z�z 7{6�����;v��v����5�G��m��-U����Uh������ѷ	�$T�4���2}�<)��	\Zȹ�$�T#�����U�j5�wK��u�;L��9[J��
�c!�7��9�h"6���pYfE��}S�n��P��4r���/4�����"��¼RIS��&wx�6ɂ ���|vT��:E��/�V`�Gi�n���1zc��%�0Z�j^��'��Bc:�|T��ދ�׼.��5\W��t�5��F�EU�AVh�s�:3��̖������1��|��mE�3�R�d��vBq�(䴢e�R5#F{��T�u�غ�t�5_6��(~���	%� �\h�� k m�#�"u��7Ru���i�8�2QK�N���A˒�.�tL��V��N�`j,� ܮ�l����E�[�*���a�VC^�X4��tz�s�g�s��s�^�����h倎ɬW5q�U�*����\C'���]66� *��@��F��v���������[���	���\���tO$a�PZ�K�X6�]�0�t4�4N�Q��)n�	O<Dp�G� sT\�L�3@6r`�X����|,1<���i�>C�.�mX���Ur)�@�����[q�u����Lj"?��N|��(˕'�z0��8����w)�n�*�e4��?�"�G��a������t������睾�~�mv}��W
�\�LxLQ��$,��K��7�J��EIC�]�ӽ.rl;�^m�KFv��l�*��ċ���n��E+��1珞[Ύ�?�E|�� �Û��>@e��V��:�}᩷|�k]+=U�f��6�v=*v-d���ܪvhg%!�J�����Cd�ǧ�9�u��*� W  8T���o��K*1I��ײ��!��D��@�%�� p�����ڀ��X����3&�=��H��\x�6.�Et���L�+x���Wg�	�����ａp�8����[`�a"���*Y{.K��4=s�)��G1��b{*pg��н�����3d�}݌��Gm�t��'�1���c5ɮ� �\S*���s�;�5p%1��q�k�����X܂����Ly�ɯ��R>OX��	��X�X����^���d�uQ�u�i6���t�ӥ���3�E�3�:@hX)P�<�ն<�冉�p  ��d�[G����4�F���<����lU��y��р��~d�3�;&��H�kv<y��F���#d�Q����U8��R��T��%��ӝ�R��� `��Vӷɷ�8���mo�FO��ߍ�z�L ��`�.���~�������nL��)�3��T�7L��k[� ^tǑSEl�������c5�gr�WW��t�w>��Y��9��ޤ���j��̵l��T�1�&�������o7�J� V�*<�?�(;ag��ɐώa��`g-�<�IH��0����5�sb=Nq��SB�6�2�]P�Q]�.w.*C�IfyÜMIҋW��$u8��kf1�`� �.JbiL��h{� �G�c�iVpD2(Y�D
x�;�t�y�F��d3Jՠ�M#c�K�v`��3��11m�@ܝ^$ċ�,f�,g�N�P����h*�8f�~�ݭS���M5w@���}����"�6g��d����\g����P@��l�W��1�v���%$���
S'7��()��)D<3y�!� }z����*���^����Lg���D���:��h91��D��1���)�f�p1J��5F������l�g=U�Y���I�L���n���>�리o��:��לYi˧���2���$G�H�&��P헫l�C����s��|�6o[l޶;4��t̮ .?�
w�����̆���L.q膝9�͂?-�_T�0u��<��+�y��/p&�dD"��]?(g�ڗ����B:��'MX�����bwF/��=5AL-V�TFm%VL�Д�l�-��>9��Wk�ThU��a�:���"%7�,�l�%���w	H��։�Z}�}�6��oȱЩ�i���TF�����5êH��bW�W�8!i\r�p�:�*�l:�̟7�>�$��4����ī�I^�/�u2���
��L��_6XU�����b�V1�V�f�C+Q홓�a�Xa@qlO��Z������9��FE�O��2�pL�Ӎ2ќ��.�q�z��z���-�#�9v����h���A�@z��T��X��[��r�.��X+Iݖ�U��jh^�����\Ov"�f�K�3\�p�c�R���P�V�	�Gm�r�8���v'�b/�]��/�f�)�*��z�ka�e+B�rNN�\���a�l�9���Gѥv^~�R;�V	�*��ӡ�\ps֨�Ή4�>� Z�A���9P6ER�Ⱥ��?vN��/C.oi�J6-wɮˌ|�%-k�~���m�o�R�ѱv�~u�9#h��'`,���Z%���u&
�麐Eb��[aW5���Y���TUNPH�=���=������/�"�@'H��.A,�� S�Rծk��	M5���[8�aql��������S���>�d�b��]�z�[�X��`����qW�\ �˽�V�2�!4z��7�x
t1h) ������j�`��?\At �1b'�5y�/S�&�a�,~���b�L�!��O)��*�RK����R[W�6������L�oo���v5��d�\,՘{W�SY�"r)��$D`�R�XS���3z�hǫ]���Y�V�G��ݺ٥�����,f���[�԰�õ�R��xX�#�8*Z�w�p���m��2�P�yֽ2�>X�hf5J�Z�8���fV+���1?������r˺�EU�$4V�]�x�:|�ǩ��b~
�z0|�D��t���w�]�պ�'$c�!ôp��-�������3=#ԴԶ�l׵�[B�����C8�6E��:�VB�5���~�7զ=u���?No�޸�D�k*-��6o@&��~Ƀ���;�������˖k�ϲ��.DM"�H�Sb��%�p���Z���!wQl[B�
����\��7�
�4����Gq�Q�y.�Pp!l���T}1���)���2c����_�{�>�2��'_,�d܎<�MY-4e�Fj�!�d��9!l: 
<�2���L�ؚ����G2-�'�����q�s��ٲ�p���Y��mQ��1����X"#y�V�tZs���n=���֘�Ǌ����Q��a[�Cc{���w��q���PP���i���`j���4X^�L���xX���+}7j�Ȑ��j�s8JhZ����O� �b����flV����#�KZ�5������E�\1@���y��������a���tg�Ѿ�y�	l>"
֪|��m�����9#�;����p߷\�Le$՘)��l��r�жQ��p�t�s�F�&O�l�վKڸ��Dk�?/u2]�?'-��~����y�[	�P {�R�O�糈`�0�}I�o�!�V�!���$3�a��킴���M*)rA޲���rR;V�i¤#&���?���#�>�e�ו<e�4p����4��@5+%9�gو�)X���{�c���H��q�
t�"}�_�����w��o��n���%�(
Pb��Ht�7�9�-�����r��?=:��Z�ǢӒ��%�x��bN-�eN��6)�?r+��O�����[��e�լU��מ	e���g m�u�	�I';��YK׿��N(��ڬ!�lh��t,����r\R��G�. ֦%�)FD�i��n��M��x8����i���v+H�F)�R��H�D�I�H��z�%1����gn��2p+��RV��9����UP��0�y ]���6y�|J����i;�nR"�
���cA����ؾb)e�E%ҹƘ�w��A-�n�Z��P���r��lh~8����P̠���3v\qI���]��Aq>Jb�0�ia�v�Fj�j��nl=�R����\���Ǝ�Z������O�i�g�V7�3�1�/؇mT9�*o�?ƣ�y��d��||ދ�u��e�����,�ǖH���m���QuyN���Ԯ2]�!�2q��a��oP	{���ɼ���`fN�+QO�t�P�T���z��i��ds��9�����;'�.V�S�����R�u(?`��m
3��z�r� �`�[�	 �� {R�Ԙ��qex�q����P� Q�f�|�ىa�V-���Z-�}�)��QJ�``v��o
H9�t�l����d`���ٞu��t��7� �b]�:�qze��r@�v�^��m�Z�<8e&)�r�`���L�m}m�6v���O�RY�i�T_�+%�U���P�!�]T5�j��^4���ص�꿃��)T%vf����2i�� v���H@�*pmU���X�i�|kY�{b6T��P�V����N�j�L6�N ?��.���-��U`~YVû�5n�Dtm�ڗ��PĈ���%`]�SN,>t�K�:�����x��V��M�Lok�`��O�a�����op�L�������zڳ����Q���\ �m����}�+�-N�*�����tʍ�d�ڷ@S�Ҥ'���;z/�睪�R�Q��_y�a?=5��C׼���7�U����|�T����y<4t31��5���_ޠߞ���Z�L�� <Iz��t�A�j/�s4�͠z�m�N^��)�AqRW] �E[6?R�24菺�^�gx.���QU����^�3�[���*y�}�a��x��)a5n�lFXq9�8)���j}~��s���7�+      �   �  x�}�In�0��+���U����!md����(pi�=Hޠoj/du������t؁��S{���\�Y
�������r�V���UbHKN���\�`��1I�ͨ&$|'C�M��p��,�v�t�<Ng'��QjI��C�愎V����?ȁ��=1]�;H�XQ��O�1�8ւ���A¬#��S��x9iA�F/=��z�)LJ�X�ۚ�A��9�V���"��� I>�2Z?���btE�6�^B�2j��#�d�#�<3&�>��f)
�?�4�֌��d<�o����"�	j/=3���	'�S��(͕��D('�_H}�d�t��V�/kyJb�v>θY�x����J�/��ω�p嬙[�!;ɲ���-X�͉
�`/ݐ<h��<����      �      x����q�@�s+�>!j�M��;�
�s&3@6�j������/�l������n���Q�%����/|�(�M�M�^-��a�6��0����x����]F��aSF�U�dY�[F��aWF�U}dTZ�j�Qk���T\N�ur��Sp-uʩ���;%�G79U�ʖS��׳ �{�$]��Q��^�*0�չc�kģ���S`tg�+0�;"TM]�
j�.��
��.�A(:o�AϺ��RI��MƕT|屔�\�5�Qs�%5��Ubs5���cs���e?"�˞J��F71�i�]安�U��951��S�[mvbtk����*ߚX]�g��v��F�{�_���`at�Wnat�׎��c>Z]妅�U�Z]塅�U�Z��nNm��9VW����*��X]�G���jcug�G���{���l���۷����^����^����^���Un:X]�C���k�PtU[����;E�y�Pt��E��ԥ�j�ܯ)�Ώ.~lc-]����y���X��lr�:������bu��}��-�e�]�      �   S  x��X�n�V]�_Ad�A^��/ЦK�C���c;A�@��i�uF�bZ6�_���zf��H��h
ې�!�qf��\G#��.�W]�'��s|�ti�}��~��4R�
�^4���{�
�0u�P��/�R�z��?иЅ933�4g����Hѵ����K���4>X�W�97S��G����7z-5�k�|��¼�âD�|��~a.�±�?̕�G�[�m���F��~-w\x�������7�������W���(�ȜsdK��O��ܲrD���#ۛ���랗*W��E� ���Ê�����taI7�	�4�-��2ҟ�ӏ<��0����ͲЪ+b��^�W��M�V�Q��Ajտ��l��S�D�-���b'"����]��aCv��<T�d����	��g���3��q�{n`KuG"��^�s�	���U�-�]�����2��873���H̀�~�F��'jl���j�\��vu'T0������h�?s?/�iED6�;�ӒX�nX���~S7J�]�=��� s�����*!��K�a���nd1��4�G�58�N�
��z��l��D�.���c>]����}��{5A��~�~�a?�<t0!=��{W$��@~��{��z��������
k/��������S$l�!�|`5��U�[��!0�'.i!Ȧ�,Ͱ�T&���1o�k�1qI�t�5d�DP<c�RQ�"_���k� ��_�����d����e���s�چ�)cp�?����Q��*�G��H	nf�վ$z3�x�欔� �S/D���v����YUR�T4K�J��W��6�I�,_Sy���,��I�>�	%��F�U�������9F�����8b���֥[���r�*<��6�0t�e(�=L�Ny��g!�H�������|Y�.��S���������[x�b��I䕨�>��;I��;YZ�+����9�A�/� H��d2�\,yM�����]��6ˣ�h>w:e ��r�P�L6�O֦7a���n��ꩁH��@���L�pu�4��|���6�*���(���>W	�-y�M�ܚˋ۵M�{�ڵ��-���J(�d~�M��r鰆�u�ή��s�-P��W�lgi��wE�!����43m|rX�8)�[���=%�ni�F��v���ě�<�&�����n/�����oN[�J��[��@�g�9w��F���If��+(0x?7���v�3��읳i���z�2�v��e���$ڑW��-���n-��B����]�����D]L�XIS��+ڭ����e��;�	���d�^���UA`�+��
~�dr樶�Q�6��X�w)V�.�) p���RO�.�U���� �@l�2�fϣ�,	0ē]�	v^7�TWW"Uf�O��wE�{'CS)�L"[��D��w���5�|�~�nd1�
��ez�l��ͅ��{s���Mt�r�?�J�����x�p�>�L�i�4�U�u��~�Mc��H���T�,�E!G�R�aW���	�H�I���:qHH��+�+�t7�c�9c�5z�?�{��y*@ R�0����R��I��H�G�Q?�bNmL�]��t�>8u����i     