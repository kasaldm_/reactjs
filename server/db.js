const {Sequelize} = require('sequelize');;
require('dotenv').config();


const PORT=5000
const DB_NAME='Reflexology'
const DB_USER='postgres'
const DB_PASSWORD='8'
const DB_HOST='localhost'
const DB_PORT=5433

const sequelize = new Sequelize(
        DB_NAME,
        DB_USER,
        DB_PASSWORD,
        {
            dialect: 'postgres',
           host: DB_HOST,
           port: DB_PORT
        }
        
);

module.exports = sequelize;