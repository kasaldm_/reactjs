const {SymptomPoint} = require('../models/models')
const ApiError = require('../error/ApiError');
const uuid = require('uuid')
const path = require('path');


class SymptomPointController {

    async create(req,res, next){
        try{
        const{ } = req.body
        const symptomPoint = await SymptomPoint.create({})
        return res.json(symptomPoint)  
        } catch (e) {
            next(ApiError.badRequest(e.message))
        } 
    }

    async getAll(req, res){
        
        let {pointId, symptomId, limit, page} = req.query
        symptomId = Number(symptomId)
        pointId = Number(pointId)
        page = page || 1
        limit = limit || 9
        let offset = page * limit - limit
        let symptomPoints;
        if (!pointId && !symptomId){
            symptomPoints = await SymptomPoint.findAndCountAll({limit, offset})
        }
        if(pointId && !symptomId){
            symptomPoints = await SymptomPoint.findAndCountAll( {where:{pointId}, limit, offset})
        }
        if(!pointId && symptomId){
            symptomPoints = await SymptomPoint.findAndCountAll( {where:{symptomId}, limit, offset})
        }
        if(pointId && symptomId){
            symptomPoints = await SymptomPoint.findAndCountAll( {where:{pointId, symptomId}, limit, offset})
        }
        return res.json(symptomPoints)
    }

    async getOne(req,res){
        const {id} = req.params
        const symptomPoint = await SymptomPoint.findOne(
            { 
                where: {id} 
            },
        )
        return res.json(symptomPoint)
    }

    
}




module.exports = new SymptomPointController()