const {Symptom} = require('../models/models')
const ApiError = require('../error/ApiError');
const uuid = require('uuid')
const path = require('path');


class SymptomController {
   
    async create(req,res, next){
        try{
            const{name_s, info, pointss, methodId} = req.body
        const symptom = await Symptom.create({name_s,info, pointss, methodId})
        return res.json(symptom)  
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }   
    }

    async getAll(req, res){
        
        let {methodId, id, limit, page} = req.query
        page = page || 1
        limit = limit || 100
        let offset = page * limit - limit
        let symptoms;
        if (!methodId && !id){
            symptoms= await Symptom.findAndCountAll({limit, offset})
        }
        if(methodId && !id){
            symptoms = await Symptom.findAndCountAll( {where:{methodId}, limit, offset})
        }
        if(!methodId && id){
            symptoms = await Symptom.findAndCountAll( {where:{id}, limit, offset})
        }
        if(methodId && id){
            symptoms = await Symptom.findAndCountAll( {where:{methodId, id}, limit, offset})
        }
        return res.json(symptoms)
    }


    async getOne(req,res){
        const {id} = req.params
        const symptom = await Symptom.findOne(
            { 
                where: {id} 

            },
        )
        return res.json(symptom)
    }

    
}

module.exports = new SymptomController()