const {Method, Symptom} = require('../models/models')
const ApiError = require('../error/ApiError');
const uuid = require('uuid')
const path = require('path');

class MethodController {

    async create(req,res, next){
        try{
        let{name_m, info, info2, info_symptom} = req.body
        
        const method = await Method.create({name_m, info, info2})
       

        if (info_symptom){
            info_symptom = JSON.parse(info_symptom)
            info_symptom.forEach(i => 
                Symptom.create({
                    title: i.title,
                    description: i.description, 
                    methodId: method.id
                })
            )
        }

         return res.json(method)  
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
         
    }

    async getAll(req, res){
        const methods = await Method.findAll()
        return res.json(methods)
    }

    async getOne(req,res){
        const {id} = req.params
        const method = await Method.findOne(
            { 
                where: {id},
               /*include: [{model: Symptom, as:'info_symptom'}]*/ 

            },
        )
        return res.json(method)
    }
}

module.exports = new MethodController()








