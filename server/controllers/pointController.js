const {Point} = require('../models/models')
const ApiError = require('../error/ApiError');
const uuid = require('uuid')
const path = require('path');


class PointController {

    async create(req,res, next){
        try{
        const{name_p,number, info, recom, xCoord, yCoord, location} = req.body
        const {img} = req.files
        let fileName = uuid.v4() + ".jpg"
        img.mv(path.resolve(__dirname, '..', 'static', fileName))
        const point = await Point.create({name_p, number, info, recom, xCoord, yCoord, location,  img: fileName})
        return res.json(point)  
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
         
    }

    async getAll(req, res){
        
        let {meridianId, symptomId, limit, page} = req.query
        page = page || 1
        limit = limit || 100
        let offset = page * limit - limit
        let points;
        if (!meridianId && !symptomId){
            points = await Point.findAndCountAll({limit, offset})
        }
        if(meridianId && !symptomId){
            points = await Point.findAndCountAll( {where:{meridianId}, limit, offset})
        }
        if(!meridianId && symptomId){
            points = await Point.findAndCountAll( {where:{symptomId}, limit, offset})
        }
        if(meridianId && symptomId){
            points = await Point.findAndCountAll( {where:{meridianId, symptomId}, limit, offset})
        }
        return res.json(points)
    }

    async getOne(req,res){
        const {id} = req.params
        const point = await Point.findOne(
            { 
                where: {id} 

            },
        )
        return res.json(point)
    }
}

module.exports = new PointController()
