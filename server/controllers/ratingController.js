const ApiError = require('../error/ApiError'); 
const { Rating, Method, Symptom } = require('../models/models'); 
const { RandomForestClassifier } = require('ml-random-forest'); 

class ForestModel { 
    constructor() { 
        this.model = new RandomForestClassifier(); 
    } 

    predict(symptomId, methodId, age, gender) { 
        const prediction = this.model.predict([[symptomId, methodId, age, gender]]); 
        return prediction; 
    } 

    train(features, labels) { 
        this.model.train(features, labels); 
    } 
} 

class RatingController { 
    constructor() { 
        this.forestModel = new ForestModel(); 
        this.trainModel();
    } 

    async trainModel() { 
        try { 
            const ratings = await Rating.findAll(); 
            if (ratings.length === 0) {  
                console.log('No ratings found. Skipping model training.');  
                return;  
            } 
            const features = ratings.map(rating => [rating.symptomId, rating.methodId, rating.age, rating.gender]); 
            const labels = ratings.map(rating => rating.rating_s); 

            this.forestModel.train(features, labels); 
            console.log('Model trained successfully'); 
        } catch (e) { 
            console.error('Error training model:', e); 
        } 
    } 

    async create(req, res, next) { 
        try { 
            const { rating_s, symptomId, methodId, age, gender } = req.body; 
            const rating = await Rating.create({ rating_s, symptomId, methodId, age, gender }); 
            return res.json(rating); 
        } catch (e) { 
            next(ApiError.badRequest(e.message)); 
        } 
    } 

    async getAll(req, res) { 
        let { symptomId, methodId, limit, page } = req.query; 
        page = page || 1; 
        limit = limit || 100; 
        let offset = page * limit - limit; 
        let ratings; 

        if (!symptomId && !methodId) { 
            ratings = await Rating.findAndCountAll({ limit, offset }); 
        } else if (symptomId && !methodId) { 
            ratings = await Rating.findAndCountAll({ where: { symptomId }, limit, offset }); 
        } else if (!symptomId && methodId) { 
            ratings = await Rating.findAndCountAll({ where: { methodId }, limit, offset }); 
        } else { 
            ratings = await Rating.findAndCountAll({ where: { symptomId, methodId }, limit, offset }); 
        } 

        return res.json(ratings); 
    } 

    async getOne(req, res) { 
        const { id } = req.params; 
        const rating = await Rating.findOne({ where: { id } }); 
        return res.json(rating); 
    } 

     predict = async (req, res, next) => { 
        try { 
            await this.trainModel()
            const { symptomId, methodId, age, gender } = req.body; 
            const method = await Method.findOne({ where: { id: methodId } }); 
            const symptom = await Symptom.findOne({ where: { id: symptomId } }); 

            if (!method || !symptom) { 
                throw new Error('Method or Symptom not found'); 
            } 

            const prediction = this.forestModel.predict(symptomId, methodId, age, gender); 
            return res.json({ prediction }); 
        } catch (e) { 
            next(ApiError.internal(e.message)); 
        } 
    } 
} 

module.exports = new RatingController();