const {Meridian} = require('../models/models')
const ApiError = require('../error/ApiError');
const uuid = require('uuid')
const path = require('path');


class MeridianController {

    async create(req,res,next){
        try{
        const{name_m,number, info, info2, info3, vse, standart} = req.body
        const {img} = req.files
        let fileName = uuid.v4() + ".jpg"
        img.mv(path.resolve(__dirname, '..', 'static', fileName))
        const meridian = await Meridian.create({name_m, number, info, info2, info3, vse, standart, img:fileName})
        return res.json(meridian)  
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
         
    }

    async getAll(req, res){
        const meridians = await Meridian.findAll()
        return res.json(meridians)
    }

    async getOne(req,res){
        const {id} = req.params
        const meridian = await Meridian.findOne(
            { 
                where: {id} 

            },
        )
        return res.json(meridian)
    }
}

module.exports = new MeridianController()
