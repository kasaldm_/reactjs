const Router = require('express')
const router = new Router()
const symptomRouter = require('./symptomRouter')
const ratingRouter = require('./ratingRouter')
const meridianRouter = require('./meridianRouter')
const methodRouter = require('./methodRouter')
const pointRouter = require('./pointRouter')
const symptomPointRouter = require('./symptomPointRouter')


router.use('/symptom', symptomRouter)
router.use('/rating', ratingRouter)
router.use('/point', pointRouter)
router.use('/meridian',meridianRouter)
router.use('/method', methodRouter)
router.use('/symptompoint', symptomPointRouter)

module.exports = router
