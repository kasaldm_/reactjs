const Router = require('express')
const router = new Router()
const meridianController = require('../controllers/meridianController')

router.post('/', meridianController.create)
router.get('/', meridianController.getAll)
router.get('/:id',meridianController.getOne)

module.exports = router

