const Router = require('express')
const router = new Router()
const symptomController = require('../controllers/symptomController')


router.post('/', symptomController.create)
router.get('/', symptomController.getAll)
router.get('/:id',symptomController.getOne)



module.exports = router
