const Router = require('express')
const router = new Router()
const symptomPointController = require('../controllers/symptomPointController')


router.post('/', symptomPointController.create)
router.get('/', symptomPointController.getAll)
router.get('/:id',symptomPointController.getOne)


module.exports = router