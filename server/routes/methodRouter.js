const Router = require('express')
const router = new Router()
const methodController = require('../controllers/methodController')


router.post('/',methodController.create)
router.get('/',methodController.getAll)
router.get('/:id',methodController.getOne)

module.exports = router