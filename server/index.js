require('dotenv').config()
const express = require('express')
const sequelize = require('./db')
const models = require('./models/models')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const router = require('./routes/index')
const errorHandler = require('./middleware/ErrorHandlingMiddleware')
const path = require('path')
const fs = require('fs'); 
const csvWriter = require('csv-write-stream');


const PORT = process.env.PORT || 5000

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.static(path.resolve(__dirname, 'static')))
app.use(fileUpload({}))
app.use('/api', router)

//Обработка ошибок
app.use(errorHandler)

// Установка имени файла для сохранения данных в формате CSV 
const filename = 'ratings.csv'; 

// Создание объекта записи CSV 
const writer = csvWriter(); 
writer.pipe(fs.createWriteStream(filename));

const fetchDataAndWriteToCSV = async () => { 
    try { 
        // Получение данных из базы данных 
        const ratings = await models.Rating.findAll(); 
        // Запись данных в CSV файл 
        ratings.forEach((rating) => { writer.write({ 
            symptom_id: rating.symptomId, 
            method_id: rating.methodId, 
            gender: rating.gender,
             age: rating.age, 
             rating: rating.rating_s });
             });
             
             writer.end(); 
             console.log('Данные успешно записаны в файл CSV.');
             } catch (error) { 
                console.error('Ошибка при получении данных из базы данных:', error);
             } };

const start = async () =>{
    try{
        await sequelize.authenticate()
        await sequelize.sync()
        await fetchDataAndWriteToCSV();
        app.listen(PORT, () => console.log(`Server started on port ${5000}`))
    } catch(e){
        console.log(e)
    }
}

start()