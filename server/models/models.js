const sequelize = require('../db')
const {DataTypes} = require('sequelize')

const Point = sequelize.define('point', {
    id:{type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name_p:{type: DataTypes.STRING, unique:true},
    number:{type: DataTypes.STRING, unique:true},
    info:{type: DataTypes.STRING},
    recom:{type: DataTypes.STRING},
    xCoord:{type: DataTypes.INTEGER},
    yCoord:{type: DataTypes.INTEGER},
    location:{type: DataTypes.STRING},
    img:{type: DataTypes.STRING}
})

const Symptom = sequelize.define('symptom', {
    id:{type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name_s:{type: DataTypes.STRING, unique:true},
    info:{type: DataTypes.STRING},
    pointss:{type: DataTypes.STRING}
})

const Rating = sequelize.define('rating', {
    rating_s: { type: DataTypes.INTEGER, allowNull: false },
    age: { type: DataTypes.INTEGER },
    gender: { type: DataTypes.INTEGER }
})

const Method = sequelize.define('method', {
    id:{type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name_m:{type: DataTypes.STRING, unique:true},
    info:{type: DataTypes.STRING},
    info2:{type: DataTypes.STRING}
})

const Meridian = sequelize.define('meridian', {
    id:{type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name_m:{type: DataTypes.STRING, unique:true},
    number:{type: DataTypes.STRING, unique:true},
    standart:{type: DataTypes.STRING},
    vse:{type: DataTypes.STRING},
    info:{type: DataTypes.STRING},
    info2:{type: DataTypes.STRING},
    info3:{type: DataTypes.STRING},
    img:{type: DataTypes.STRING}
})

const SymptomPoint = sequelize.define('symptom_point', {
    id:{type:DataTypes.INTEGER, primaryKey:true, autoIncrement:true},
})

Meridian.hasMany(Point)
Point.belongsTo(Meridian)

Method.hasMany(Symptom, {as: 'info_symptom'})
Symptom.belongsTo(Method)

Symptom.hasMany(Rating)
Rating.belongsTo(Symptom)

Method.hasMany(Rating)
Rating.belongsTo(Method)

Symptom.belongsToMany(Point, {through: SymptomPoint})
Point.belongsToMany(Symptom, {through:SymptomPoint })

/*Symptom.hasMany(SymptomPoint)
SymptomPoint.belongsTo(Symptom)

Point.hasMany(SymptomPoint)
SymptomPoint.belongsTo(Point)*/

module.exports = {
    Point,
    Symptom,
    Method,
    Meridian,
    SymptomPoint,
    Rating
}