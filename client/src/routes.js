import { Component } from "react";
import Main from "./pages/main";
import Point from "./pages/point";
import Symptom from "./pages/symptom";
import Meridian from "./pages/meridian";
import Method from "./pages/method";
import PredictionForm from "./pages/PredictionForm";
import { MAIN_ROUTE, POINT_ROUTE, SYMPTOM_ROUTE, MERIDIAN_ROUTE,METHOD_ROUTE, PREDICTION_ROUTE } from "./utils/consts";

export const publicRoutes = [
   { path: MAIN_ROUTE,
    Component: Main
    },
    { path: POINT_ROUTE,
        Component: Point
        },
        { path: SYMPTOM_ROUTE,
            Component: Symptom
            },
            { path: MERIDIAN_ROUTE,
                Component: Meridian
                },
                { path: METHOD_ROUTE,
                    Component: Method
                    },
                    { path: PREDICTION_ROUTE,
                        Component: PredictionForm
                        },
]