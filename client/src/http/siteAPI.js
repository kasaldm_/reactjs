import {$host} from "./index";

export const fetchMethod = async () => {
    try {
        const response = await $host.get('api/method');
        const data = response.data; // Предполагается, что данные находятся в свойстве data объекта ответа
       

        return data;
    } catch (error) {
        console.error("Ошибка при получении данных:", error);
        throw error; // Пробрасывание ошибки для обработки в вызывающем коде
    }
}


export const fetchPoint = async () => {
    try {
        const response = await $host.get('api/point');
        const data = response.data.rows; // Предполагается, что данные находятся в свойстве data объекта ответа
       

        return data;
    } catch (error) {
        console.error("Ошибка при получении данных:", error);
        throw error; // Пробрасывание ошибки для обработки в вызывающем коде
    }
}




export const fetchMeridian = async () => {
    try {
        const response = await $host.get('api/meridian');
        const data = response.data; // Предполагается, что данные находятся в свойстве data объекта ответа
       

        return data;
    } catch (error) {
        console.error("Ошибка при получении данных:", error);
        throw error; // Пробрасывание ошибки для обработки в вызывающем коде
    }
};



export const fetchSymptom = async () => {
    try {
        const response = await $host.get('api/symptom');
        const data = response.data.rows; // Предполагается, что данные находятся в свойстве data объекта ответа
       

        return data;
    } catch (error) {
        console.error("Ошибка при получении данных:", error);
        throw error; // Пробрасывание ошибки для обработки в вызывающем коде
    }
};

export const fetchOneMeridian = async (id) => {
    try {
        const response = await $host.get('api/meridian/' + id);
        const data = response.data; // Предполагается, что данные находятся в свойстве data объекта ответа
       

        return data;
    } catch (error) {
        console.error("Ошибка при получении данных:", error);
        throw error; // Пробрасывание ошибки для обработки в вызывающем коде
    }
};