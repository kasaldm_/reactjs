import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {Routes, Route, Navigate} from 'react-router-dom'
import Main from './pages/main'
import { publicRoutes } from './routes';

const AppRouter = () => {
    return(
        <Routes>
            {publicRoutes.map(({path, Component}) =>
            <Route key={path} path={path} element={<Component/>} exact/>
            )}
            <Route path="*" element={<Main/>}/>
        </Routes>
    );
};

export default AppRouter;

