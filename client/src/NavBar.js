import React from "react";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import SymptomList from "./pages/symptomList";
import 'bootstrap/dist/css/bootstrap.min.css';


function NavBar() {
 

  return (
    <Navbar expand="lg" className="navbar fixed-top navbar-light bg-light" >
      <Container>
        <Navbar.Brand href="/main">Reflexology</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link href="/meridian">Меридианы</Nav.Link>
            <Nav.Link href="/point">Точки</Nav.Link>
            <Nav.Link href="/symptom">Симптомы</Nav.Link>
            <Nav.Link href="/method">Методы</Nav.Link>
            <Nav.Link href="/predict">Предсказание</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;