import React, { useEffect, useState } from "react";
import axios from "axios";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import otnoshenie from './otnoshenie.png'; 

const MeridianDescription = ({diseaseId}) => {
    
    const [site, setMeridianData] = useState(null)
    const [pointData, setPointData] = useState(null);
    const [selectedPoint, setSelectedPoint] = useState(null);

    if (site != null){
      var infoSplitList = site.standart.split('$'); 
    }
    useEffect(() => {
      setSelectedPoint(null);
        const fetchMeridianData = async () => {
          try {
            const response = await axios.get(`http://localhost:5000/api/meridian/${diseaseId}`);
            setMeridianData(response.data);

            const pointResponse = await axios.get(`http://localhost:5000/api/point?meridianId=${diseaseId}`); 
        setPointData(pointResponse.data.rows); 
 
            
          } catch (error) {
            console.error('Ошибка при загрузке данных:', error);
          }
        };

        if(diseaseId){
                fetchMeridianData();
        }
      }, [diseaseId]);

      const handlePointClick = (point) => {
        if(selectedPoint && selectedPoint.id === point.id){
          setSelectedPoint(null);
        } else{
        setSelectedPoint(point);
      }
      };
    
      

      return (
        <div>
          {site ? (
            <Container>
              <h1>{site.name_m}</h1>
              <p>{site.number}</p>
              <Row className="mt-1">
              <Col md='5'>
                <Image  src={ process.env.REACT_APP_API_URL + site.img}/>
                {selectedPoint && (
                <div
                  className="selected-point-marker"
                  style={{
                    position: "absolute",
                    top: selectedPoint.yCoord,
                    left: selectedPoint.xCoord,
                    width: "10px",
                    height: "10px",
                    backgroundColor: "blue",
                    borderRadius: "50%",
                    animation: "blinking 1s infinite",
                  }}
                ></div>
              )}
              </Col>
              <Col md='7'>
              <h3>Точки меридиана:</h3> 
              <div>
              {pointData &&  pointData
    .sort((a, b) => parseInt(a.number) - parseInt(b.number)) 
    .map((point) => ( 
      <li  key={point.id} 
        onClick={() => handlePointClick(point)}  
        style={{ cursor: "pointer", backgroundColor: selectedPoint === point ? "#007bff" : "transparent", color: selectedPoint === point ? "#ffffff" : "#000000", border: "3px solid #dee2e6", borderRadius: "5px", marginBottom: "2px", padding: "0.5rem 1rem" }}
        className="list-group-item d-flex justify-content-between align-items-center"
      > 
       <span>{point.number} ({point.name_p}) </span> <span>{point.info}</span></li> 
    ))}
              </div>
              
              {selectedPoint && (
                <div className="selected-point-info">
                  <h3>Информация о выбранной точке:</h3>
                  <p>Название: {selectedPoint.name_p}</p>
                  <p>Номер: {selectedPoint.number}</p>
                  <p>Расположение: {selectedPoint.location}</p>
                  <p>{selectedPoint.recom}</p>
                </div>
              )}
              <h3 style={{padding:20}}>Информация меридиана:</h3> 
               {infoSplitList.map(item =><p>{item}</p>)}
               <h3 style={{padding:10}}>Симптомы меридиана:</h3> 
              <p>{site.info} {site.info2} {site.info3}</p>
              </Col>
              </Row>
            </Container>
          ) : (
            <div>
            <h4 style={{padding:10, fontFamily:'Lato'}}>
            Наиболее часто при лечении методом рефлексотерапии используют точки, расположенные на линиях, 
            которые древневосточные врачи называли меридианами. 
            Они обозначали их, используя названия органов (меридиан легких, сердца, желудка и
            т. д.). 
            Также меридиан обозначают римской цифрой или одной или двумя буквами английского, 
            французского или немецкого алфавита.</h4>
            <h5 style={{padding:10, fontFamily:'Lato', textAlign: 'center'}}> Взаимоотношение болевых ощущений и меридианов:  </h5>
             <img  className="displayed" src={otnoshenie} alt="otnoshenie"/>
             
             </div>
          )}
        </div>
      );
};

export default MeridianDescription;

