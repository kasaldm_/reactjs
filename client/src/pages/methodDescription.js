import React, { useEffect, useState } from "react";
import axios from "axios";

const MethodDescription = ({diseaseId}) => {
    
    const [site, setMethodData] = useState(null)

    if (site != null){
      var infoSplitList = site.info2.split('$'); 
    }

    useEffect(() => {
        const fetchMethodData = async () => {
          try {
            const response = await axios.get(`http://localhost:5000/api/method/${diseaseId}`);
            setMethodData(response.data);
          } catch (error) {
            console.error('Ошибка при загрузке данных:', error);
          }
        };

        if(diseaseId){
                fetchMethodData();
        }
      }, [diseaseId]);
    
      return (
        <div>
          {site ? (
            <div>
              <h1 style={{padding:10}}>{site.name_m}</h1>
              {infoSplitList.map(item =><p>{item}</p>)}
            </div>
          ) : (
            <h4>Выбери метод...</h4>
          )}
        </div>
      );
};

export default MethodDescription;