import React, { useEffect, useState } from "react";
import axios from "axios";
import SymptomModal from "./symptomModal";
import { Button } from "react-bootstrap";
import {$host} from "../http/index.js";

const SymptomDescription = ({ diseaseId }) => {
  const [site, setSymptomData] = useState(null);
  const [methodData, setMethodData] = useState(null);
  const [spData, setSPData] = useState([]);
  const [selectedPointIndex, setSelectedPointIndex] = useState(null);
  const [modalShow, setModalShow] = useState(false);
  const [selectedSymptom, setSelectedSymptom] = useState(null);
  const [selectedMethodId, setSelectedMethodId] = useState(null);

  if (methodData != null){
    var infoSplitList = methodData.info2.split('$'); 
  }

  useEffect(() => {
    setSelectedPointIndex(null);
    const fetchSymptomData = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/api/symptom/${diseaseId}`);
        setSymptomData(response.data);

        const methodResponse = await axios.get(`http://localhost:5000/api/method/${response.data.methodId}`);
        setMethodData(methodResponse.data);

        const spResponse = await axios.get(`http://localhost:5000/api/symptompoint?symptomId=${diseaseId}`);

        const pointsData = await Promise.all(spResponse.data.rows.map(async (symptomPoint) => {
          const pointResponse = await axios.get(`http://localhost:5000/api/point/${symptomPoint.pointId}`);
          return pointResponse.data;
        }));
        setSPData(pointsData);
      } catch (error) {
        console.error('Ошибка при загрузке данных:', error);
      }
    };

    if (diseaseId) { fetchSymptomData(); }
  }, [diseaseId]);

  const handleClick = () => {
    setMethodData(prevMethodData => ({
      ...prevMethodData,
      showInfo2: !prevMethodData.showInfo2
    }));
  };

  const handleClickPoint = (index) => {
    if (selectedPointIndex === index) {
      setSelectedPointIndex(null); 
    } else {
      setSelectedPointIndex(index); 
    }
  };

  const handleOpenModal = (symptom) => {
    if (symptom) {
      setSelectedMethodId(symptom.methodId); // Сохраняем ID метода
      setSelectedSymptom(symptom);
      setModalShow(true);
    } else {
      console.error('Ошибка: symptom не определен');
    }
  };

  const saveRatingToServer = async (symptomId,  methodId, rating_s, age, gender) => {
    try {
      await $host.post('api/rating', { symptomId, methodId, rating_s, age, gender });
    } catch (error) {
      console.error('Ошибка при сохранении оценки:', error);
    }
  };

  const handleSaveRating = async (symptomId, methodId, rating_s, age, gender) => {
   await saveRatingToServer(symptomId, methodId, rating_s, age, gender);
  };

 
  return (
    <div>
      {site ? (
        <div>
          <h1>{site.name_s}</h1>
          {methodData && (
            <div>
              <p onClick={handleClick} style={{ cursor: 'pointer', textDecoration: 'none', borderBottom: '1px solid transparent' }}
                 onMouseEnter={(e) => e.target.style.borderBottom = '1px solid black'}
                 onMouseLeave={(e) => e.target.style.borderBottom = '1px solid transparent'}>
                {methodData.name_m}
              </p>
              {methodData.showInfo2 && <p>{infoSplitList.map(item =><p>{item}</p>)}</p>}
            </div>
          )}
           
          <h2>Точки симптома:</h2>
          <div>
            {spData && spData.length > 0 ? (
              <div>
                {spData.map((item, index) => ( 
  <div key={index}> 
    <h6 onClick={() => handleClickPoint(index)} style={{ 
      cursor: "pointer", 
      backgroundColor: selectedPointIndex === index ? "#007bff" : "transparent", 
      color: selectedPointIndex === index ? "#ffffff" : "#000000", 
      border: "3px solid #dee2e6", 
      borderRadius: "5px", 
      marginBottom: "2px", 
      padding: "0.5rem 1rem" 
    }} 
        className="list-group-item d-flex justify-content-between align-items-center"> 
      <span>{item.number} ({item.name_p}) </span> <span>{item.info}</span> 
    </h6> 
  </div> 
))}
                
              </div>
            ) : (
              <p>Нет данных о точках симптома.</p>
            )}
          </div>
        </div>
      ) : (
        <div>
           
            <h4 style={{padding:10, fontFamily:'Lato', textAlign: 'center', color:'red'}}>В этих случаях взаимодействовать на точки нельзя, и иного варианта быть не может! </h4>
            <h5 style={{padding:20, fontFamily:'Lato', textAlign: 'center'}}>
            Противопоказания: беременность, период обострения хронических заболеваний, признаки ОРВИ, инфекции и онкологические заболевания.
              </h5>
             </div>
      )}
      {selectedPointIndex !== null && (
        <div>
        <h3>Описание точки: </h3> 
        <div dangerouslySetInnerHTML={{ __html: `Номер: ${spData[selectedPointIndex].number} <br> ${spData[selectedPointIndex].recom} <br><br> Расположение: ${spData[selectedPointIndex].location}  ` }} />
        <img src={ process.env.REACT_APP_API_URL + spData[selectedPointIndex].img} alt={`Изображение точки ${spData[selectedPointIndex].number}`} />
     </div>
      )}
      <div> <p style={{padding:10}}>
      {site && <Button onClick={() => handleOpenModal(site)}>Оценить метод воздействия</Button>}
      {selectedSymptom && (
        <SymptomModal 
          show={modalShow} 
          handleClose={() => setModalShow(false)} 
          symptom={selectedSymptom} 
          methodId={selectedMethodId}
          handleSave={handleSaveRating} 
        />
      )}
      </p>
    </div>
    </div>
  );
};

export default SymptomDescription;