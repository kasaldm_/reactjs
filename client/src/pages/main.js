import React from "react";
import {Container} from "react-bootstrap";

import './main.css';

const Main = () => {
    return(
      
        <div className = "bg">
          
          
          <Container className="mt-5">
          
            <div className="white" style={{padding:65}} >
              <h1>Помощник врача-рефлексотерапевта</h1>
            </div>
            <div className="white" style={{padding:34}} >

              <h5 style={{color: "black"}}>• </h5>
            </div>
            <div className="niz"  >
              <h4>Человеческий организм - единая</h4>
              <h4>система, в которой всё взаимосвязанно</h4>
              <h6 style={{color: "black"}}>. </h6>
              
              <h5>Рефлексотерапия – комплекс приемов, в основе которых</h5>
              <h5>лежит применение с лечебной целью различных физических</h5>
              <h5>факторов воздействия на определенные точечные участки</h5>
              <h5>поверхности тела (точки акупунктуры)</h5>
              <h6 style={{color: "black"}}>. </h6>
              <h6 style={{color: "black"}}>. </h6>
              <h6 style={{color: "black"}}>. </h6>
              <h6 style={{color: "black"}}>. </h6>
              <h6 style={{color: "black"}}>. </h6>
              
            </div>

            <div style={{padding:11}} >
            <h6 style={{color: "black"}}>. </h6>
              
            </div>
            
          </Container>
    
        </div>
      
      
        
    );
};

export default Main;