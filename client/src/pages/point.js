import React, { useContext, useEffect, useState } from "react";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import PointList from './pointList';
import PointDescription from './pointDescription';
import { Context } from "../index";
import { fetchPoint } from "../http/siteAPI";
import { observer } from "mobx-react-lite";

const Point = observer(() => {
    const {site} = useContext(Context)  
    const [diseaseId, setDiseaseId] = useState(null)  
    const [pointLoaded, setPointsLoaded] = useState(false);
     
    useEffect(()=>{
      fetchPoint().then(data => {site.setPoints(data);  setPointsLoaded(true)} )
       }, [])
     
      return(
        <div style={{padding:60,}}>
          <Container>
          <Row className="mt-2">
            <Col md='3'> {pointLoaded && <PointList onSelectPoint={setDiseaseId}/>}</Col>
            <Col md='9'><PointDescription diseaseId={diseaseId}/></Col>
          </Row>
        </Container>
        </div> );
  });

export default Point;