import React, { useState } from 'react'; 
// import axios from 'axios'; 
import {$host} from "../http/index.js";
 
const PredictionForm = () => { 
    const [symptomId, setSymptomId] = useState(''); 
    const [methodId, setMethodId] = useState(''); 
    const [age, setAge] = useState(''); 
    const [gender, setGender] = useState(1); 
    const [prediction, setPrediction] = useState(null); 
 
    const handleSubmit = async (e) => { 
        e.preventDefault(); 
         
        try { 
            const response = await $host.post('api/rating/predict', { 
                symptomId: parseInt(symptomId), 
                methodId: parseInt(methodId), 
                age: parseInt(age), 
                gender:parseInt(gender)  
            }); 
            setPrediction(response.data.prediction); 
        } catch (error) { 
            console.error('There was an error making the prediction request!', error); 
        } 
    }; 
 
    return ( 
        <div> 
            <h2>Rating Prediction</h2> 
            <form onSubmit={handleSubmit}> 
                <div> 
                    <label>Symptom ID:</label> 
                    <input 
                        type="number" 
                        value={symptomId} 
                        onChange={(e) => setSymptomId(e.target.value)} 
                        required 
                    /> 
                </div> 
                <div> 
                    <label>Method ID:</label> 
                    <input 
                        type="number" 
                        value={methodId} 
                        onChange={(e) => setMethodId(e.target.value)} 
                        required 
                    /> 
                </div> 
                <div> 
                    <label>Age:</label> 
                    <input 
                        type="number" 
                        value={age} 
                        onChange={(e) => setAge(e.target.value)} 
                        required 
                    /> 
                </div> 
                <div> 
                    <label>Gender:</label> 
                    <select value={gender}  
                    onChange={(e) => setGender(e.target.value)} required > 
                         
                          <option value={1}>Male</option>  
                          <option value={0}>Female</option> 
                     </select> 
                     
                </div> 
                <button type="submit">Predict</button> 
            </form> 
            {prediction !== null && ( 
                <div> 
                    <h3>Prediction:</h3> 
                    <p>{prediction}</p> 
                </div> 
            )} 
        </div> 
    ); 
}; 
 
export default PredictionForm;