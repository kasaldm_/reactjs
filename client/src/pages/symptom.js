import React, { useContext, useEffect, useState } from "react";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SymptomList from './symptomList';
import SymptomDescription from './symptomDescription';
import { Context } from "../index";
import { fetchSymptom } from "../http/siteAPI";
import { observer } from "mobx-react-lite";

const Symptom = observer(() => {
  const {site} = useContext(Context)  
  const [diseaseId, setDiseaseId] = useState(null)  
  const [symptomLoaded, setSymptomsLoaded] = useState(false);
   
  useEffect(()=>{
    fetchSymptom().then(data => {site.setSymptoms(data);  setSymptomsLoaded(true)} )
  }, [])

  
    return(
      <div style={{padding:60,}}>
        <Container>
        <Row className="mt-2">
          <Col md='3'> {symptomLoaded && <SymptomList onSelectSymptom={setDiseaseId}/>}</Col>
          <Col md='9'><SymptomDescription diseaseId={diseaseId}/></Col>
        </Row>
      </Container>
      </div>
);
});

export default Symptom;