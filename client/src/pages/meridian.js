import React, { useContext, useEffect, useState } from "react";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import MeridianList from "./meridianList";
import MeridianDescription from './meridianDescription';
import { Context } from "../index";
import { fetchMeridian } from "../http/siteAPI";
import { observer } from "mobx-react-lite";


const Meridian = observer(() => {
  const {site} = useContext(Context)  
  const [diseaseId, setDiseaseId] = useState(null)  
  const [meridianLoaded, setMeridiansLoaded] = useState(false);
   
  useEffect(()=>{
    fetchMeridian().then(data => {site.setMeridians(data);  setMeridiansLoaded(true)} )
  }, [])
   
    return(
      <div style={{padding:60,}}>
        <Container>
        <Row className="mt-2">
          <Col md='2'> {meridianLoaded && <MeridianList onSelectMeridian={setDiseaseId}/>}</Col>
          <Col md='10'><MeridianDescription diseaseId={diseaseId}/></Col>
        </Row>
      </Container>
      </div>
    );
});

export default Meridian;