import React, { useState } from "react";
import ListGroup from 'react-bootstrap/ListGroup';
import { Context } from "../index";
import { useContext } from "react";

const PointList = ({onSelectPoint})  => {
    const {site} = useContext(Context);
    const [searchQuery, setSearchQuery] = useState("");
    const handleSelectPoint = point => {
      onSelectPoint(point.id);
      site.setSelectedPoint(point);
    };

    const filteredPoints = site.points.filter(point => 
      point.name_p.toLowerCase().includes(searchQuery.toLowerCase())||
  (point.info && point.info.toLowerCase().includes(searchQuery.toLowerCase()))||
  (point.number && point.number.toLowerCase().includes(searchQuery.toLowerCase())));
  const sortedPoint = filteredPoints.sort((a,b) => a.name_p.localeCompare(b.name_p));

    if (!site || !Array.isArray(site.points)) {
      return null; // or a loading spinner or error message
    }

    return(
<div >  
<input  type="text"  class="form-control" style={{ marginBottom:5}} placeholder="Поиск точек" value={searchQuery} 
    onChange={e => setSearchQuery(e.target.value)} />
  <div style={{ height: "575px", overflowY: "auto" }} className="custom-scrollbar">
    <ListGroup >
      {sortedPoint.map(point=>(
        <ListGroup.Item 
            style={{cursor: 'pointer'}}
            active ={point.id === site.selectedPoint.id}
            onClick={() => handleSelectPoint(point)}
            key={point.id}> 
            {point.name_p} ({point.number})
        </ListGroup.Item>))}
    </ListGroup>
    </div>
    </div> 
    );};

export default PointList;