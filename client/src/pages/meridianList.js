import React, { useState } from "react";
import ListGroup from 'react-bootstrap/ListGroup';
import { Context } from "../index";
import { useContext } from "react";

const MeridianList  = ({onSelectMeridian})  => {
    const {site} = useContext(Context);
    const [searchQuery, setSearchQuery] = useState("");
    const handleSelectMeridian = meridian => {
      onSelectMeridian(meridian.id);
      site.setSelectedMeridian(meridian); };

    const filteredMeridians = site.meridians.filter(meridian => 
      meridian.name_m.toLowerCase().includes(searchQuery.toLowerCase())||
  (meridian.number && meridian.number.toLowerCase().includes(searchQuery.toLowerCase()))||
  (meridian.info3 && meridian.info3.toLowerCase().includes(searchQuery.toLowerCase()))||
  (meridian.info2 && meridian.info2.toLowerCase().includes(searchQuery.toLowerCase()))||
  (meridian.info && meridian.info.toLowerCase().includes(searchQuery.toLowerCase()))||
  (meridian.vse && meridian.vse.toLowerCase().includes(searchQuery.toLowerCase())))
  .sort((a,b) => a.id - b.id);

    if (!site || !Array.isArray(site.meridians)) {
      return null; // or a loading spinner or error message
    }

    return(
      <div>
<input  type="text"  class="form-control" style={{ marginBottom:5}} placeholder="Поиск меридианов" value={searchQuery} 
    onChange={e => setSearchQuery(e.target.value)} />

    <ListGroup >
      {filteredMeridians.map(meridian=>(
        <ListGroup.Item 
            style={{cursor: 'pointer'}}
            active ={meridian.id === site.selectedMeridian.id}
            onClick={() => handleSelectMeridian(meridian)}
            key={meridian.id}> 
            {meridian.name_m} 
        </ListGroup.Item>))}
    </ListGroup>
    </div>
    );
};

export default MeridianList ;