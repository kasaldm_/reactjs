import React from "react";
import ListGroup from 'react-bootstrap/ListGroup';
import { Context } from "../index";
import { useContext } from "react";

const MethodList  = ({onSelectMethod})  => {
    const {site} = useContext(Context);
    const handleSelectMethod = method => {
      onSelectMethod(method.id);
      site.setSelectedMethod(method);};

    if (!site || !Array.isArray(site.methods)) {
      return null; // or a loading spinner or error message
    }

    return(
      <div style={{ height: "620px", overflowY: "auto" }} className="custom-scrollbar">
    <ListGroup >
      {site.methods.sort((a,b) => a.name_m.localeCompare(b.name_m)).map(method=>( 
        <ListGroup.Item 
            style={{cursor: 'pointer'}}
            active ={method.id === site.selectedMethod.id}
            onClick={() => handleSelectMethod(method)}
            key={method.id}> 
            {method.name_m} 
        </ListGroup.Item>))}
    </ListGroup>
    </div>
    );
};

export default MethodList ;