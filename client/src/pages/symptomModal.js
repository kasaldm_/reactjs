import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

const SymptomModal = ({ show, handleClose, symptom, methodId, handleSave }) => {
  const [rating_s, setRating] = useState(5);
  const [age, setAge] = useState(''); 
  const [gender, setGender] = useState(1);

  const handleRatingChange = (e) => { setRating(e.target.value); };
  const handleAgeChange = (e) => { setAge(e.target.value); };
  const handleGenderChange = (e) => {setGender(e.target.value); };

  const saveRating = () => {
    if (symptom && symptom.id && methodId) { 
      handleSave(symptom.id, methodId, rating_s, age, gender); 
      handleClose();
    } else {
      console.error('Ошибка: symptom, symptom.id или methodId не определены');
    }
  };

  return ( 
    <Modal show={show} onHide={handleClose}> 
      <Modal.Header closeButton> 
        <Modal.Title>Оценка эффекта воздействия на симптом: {symptom.name_s}</Modal.Title> 
      </Modal.Header> 
      <Modal.Body> 
        <Form> 
          <Form.Group controlId="formBasicRange"> 
            <Form.Label>Оцените эффект:</Form.Label> 
            <Form.Control  
              type="range"  
              min="1"  
              max="10"  
              value={rating_s}  
              onChange={handleRatingChange}  
            /> 
            <Form.Text className="text-muted">{rating_s}</Form.Text> 
          </Form.Group> 
          <Form.Group controlId="formAge">
            <Form.Label>Возраст:</Form.Label>
            <Form.Control 
              type="number" 
              value={age} 
              onChange={handleAgeChange} 
            />
          </Form.Group>
          <Form.Group controlId="formGender">
            <Form.Label>Пол:</Form.Label>
            <Form.Control 
              as="select" 
              value={gender} 
              onChange={handleGenderChange}
            >
              
              <option value={1}>Мужской</option>
              <option value={0}>Женский</option>
              
            </Form.Control>
          </Form.Group>
        </Form> 
      </Modal.Body> 
      <Modal.Footer> 
        <Button variant="secondary" onClick={handleClose}> 
          Закрыть 
        </Button> 
        <Button variant="primary" onClick={saveRating}> 
          Сохранить 
        </Button> 
      </Modal.Footer> 
    </Modal> 
  ); 
};

export default SymptomModal;