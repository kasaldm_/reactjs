import React, { useEffect, useState } from "react";
import axios from "axios";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import yn from './yn.jpg'; 




const PointDescription = ({diseaseId}) => {
    
    const [site, setPointData] = useState(null)
    const [meridianData, setMeridianData] = useState(null);
    
    useEffect(() => {
        const fetchPointData = async () => {
          try {
            const response = await axios.get(`http://localhost:5000/api/point/${diseaseId}`);
            setPointData(response.data);

            const meridianResponse = await axios.get(`http://localhost:5000/api/meridian/${response.data.meridianId}`); 
        setMeridianData(meridianResponse.data); 

          } catch (error) {
            console.error('Ошибка при загрузке данных:', error);
          }
        };

        if(diseaseId){
                fetchPointData();
        }
      }, [diseaseId]);

      const handleClick = () => {
        setMeridianData(prevMeridianData => ({
          ...prevMeridianData,
          showStandart: !prevMeridianData.showStandart
        }));
      }; 
    
      return (
        <div>
          {site ? (
             <Container>
             <h1>{site.name_p}</h1>
             <p>Номер: {site.number}</p>
             {meridianData &&
             <div>
             <p onClick={handleClick} style={{ cursor: 'pointer', textDecoration: 'none', borderBottom: '1px solid transparent' }} 
   onMouseEnter={(e) => e.target.style.borderBottom = '1px solid black'} 
   onMouseLeave={(e) => e.target.style.borderBottom = '1px solid transparent'}>
   Точка входит в {meridianData && meridianData.name_m} 
</p>
          {meridianData.showStandart && <p>{meridianData.standart.split('$')}</p>}
          </div>
          }
             <h6>{site.info}</h6>
             <p>Расположение: {site.location}</p>
             
             <Row className="mt-1">
             <Col md='6'>
               <Image  src={ process.env.REACT_APP_API_URL + site.img}/>
             </Col>
             <Col md='6'>
              
             </Col>
             </Row>
           </Container>
          ) : (
            <div>
            <h4 style={{padding:20, fontFamily:'Lato'}} > Акупунктурная точка – это участок кожи и подкожной основы, который имеет комплекс взаимосвязанных микроструктур 
            (кровеносных сосудов, нервов, клеток соединительной ткани и др.), создающих активную зону. <br/>
            Эта зона оказывает влияние на нервные окончания и осуществляет связь между участком
            кожи и внутренним органом. Область точки характеризуется усиленным поглощением кислорода,
             более высокой по сравнению с рядом лежащими участками кожи температурой, более низким 
             электрическим сопротивлением.</h4> 
            <h5 style={{fontFamily:'Open Sans'}}>
            Признаком правильного нахождения точки акупунктуры является появление при надавливании на 
            область ее расположения особых ощущений, которые называются предусмотренными. Это те ощущения, 
            которые ожидает получить врач при воздействии на точку. Пациент отмечает появление в области точки
            ощущения ломоты, тяжести, распирания, болезненности, онемения, укола, о чем он и сообщает врачу. 
            В зависимости от клинической картины заболевания
            врач ставит задачу добиться сильных, слабых предусмотренных ощущений или ощущений средней силы. 
            Иногда при воздействии на точку возникает иррадиация возникшего ощущения вверх, вниз, в сторону. 
            </h5>

            <h4 style={{padding:20, fontFamily:'Open Sans'}}>Расположение каждой точки принято искать через 
            признаки тела и расстояние по цуням. Так выглядят цуни:</h4>
            
            
            <img  className="displayed" src={yn} alt="yn"/>
            
             
            <h5 style={{fontFamily:'Open Sans'}} > У каждой точки прописаны цифро-цифрового обозначения.
            Такое освоение точек следует признать более простым и доступным. 
            Использование цифро-цифрового обозначения точек при составлении методических пособий и атласов
            в значительной степени облегчает процесс обучения врачей, впервые осваивающих метод рефлексотерапии.
           Такое обозначение точек акупунктуры является целесообразным в методической литературе, расчитанной
            на широкий круг читателей, и способствует распростанению применения данного метода среди населения. 
           </h5>
           </div>
          )}
        </div>
      );
};

export default PointDescription;