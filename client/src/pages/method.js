import React, { useContext, useEffect, useState } from "react";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import MethodList from './methodList';
import MethodDescription from './methodDescription';
import { Context } from "../index";
import { fetchMethod } from "../http/siteAPI";
import { observer } from "mobx-react-lite";

const Method = observer(() => {
  const {site} = useContext(Context)  
  const [diseaseId, setDiseaseId] = useState(12)  
  const [methodLoaded, setMethodsLoaded] = useState(false);
   
  useEffect(()=>{
    fetchMethod().then(data => {site.setMethods(data);  setMethodsLoaded(true)} )
  }, [])
   
    return(
      <div style={{padding:60,}}>
        <Container>
        <Row className="mt-2">
          <Col md='3'> {methodLoaded && <MethodList onSelectMethod={setDiseaseId}/>}</Col>
          <Col md='9'><MethodDescription diseaseId={diseaseId}/></Col>
        </Row>
      </Container>
      </div>
    );
});

export default Method;