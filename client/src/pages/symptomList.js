import React, { useState } from "react";
import ListGroup from 'react-bootstrap/ListGroup';
import { Context } from "../index";
import { useContext } from "react";

const SymptomList = ({onSelectSymptom})  => {
    const {site} = useContext(Context);
    const [searchQuery, setSearchQuery] = useState("");
    const handleSelectSymptom = symptom => {
      onSelectSymptom(symptom.id);
      site.setSelectedSymptom(symptom);
    };

    const filteredSymptoms = site.symptoms.filter(symptom => 
    symptom.name_s.toLowerCase().includes(searchQuery.toLowerCase())||
  (symptom.info && symptom.info.toLowerCase().includes(searchQuery.toLowerCase()))||
  (symptom.pointss && symptom.pointss.toLowerCase().includes(searchQuery.toLowerCase())));
  const sortedSymptom = filteredSymptoms.sort((a,b) => a.name_s.localeCompare(b.name_s));

    if (!site || !Array.isArray(site.symptoms)) {
      return null; // or a loading spinner or error message
}
  
    return(
<div >  
<input  type="text"  className="form-control" 
style={{ marginBottom:5}} placeholder="Поиск симптомов" 
value={searchQuery} onChange={e => setSearchQuery(e.target.value)} />
  <div style={{ height: "575px", overflowY: "auto" }} className="custom-scrollbar">
    <ListGroup >
      {sortedSymptom.map(symptom=>(
        <ListGroup.Item 
            style={{cursor: 'pointer'}}
            active ={symptom.id === site.selectedSymptom.id}
            onClick={() => handleSelectSymptom(symptom)}
            key={symptom.id}> 
            {symptom.name_s} 
        </ListGroup.Item>))}
    </ListGroup>
    </div>
    </div> 
    ); 
};

export default SymptomList;