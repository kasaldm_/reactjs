import {makeAutoObservable, makeObservable} from "mobx";

export default class Site {
    constructor() {
        this._meridians= [
        ]
        this._symptoms = [  
        ] 
        this._points = [
        ]
        this._methods = [ 
        ]
        this._ratings = [ 
        ]
        
        this._selectedMethod = {}
        this._selectedMeridian = {}
        this._selectedSymptom = {}
        this._selectedRating = {}
        this._selectedPoint = {}
        makeAutoObservable(this)
    }


setMeridians(meridians) {
    this._meridians = meridians
}
setPoints(points) {
    this._points = points
}
setMethods(methods) {
    this._methods = methods
}
setSymptoms(symptoms) {
    this._symptoms = symptoms
}
setRatings(ratings) {
    this._ratings = ratings
}

setSelectedSymptom(symptoms){
    this._selectedSymptom = symptoms
}
setSelectedMethod(methods){
    this._selectedMethod = methods
}
setSelectedMeridian(meridians){
    this._selectedMeridian = meridians
}
setSelectedPoint(points){
    this._selectedPoint = points
}

setSelectedRating(ratings){
    this._selectedRating = ratings
}

get meridians() {
    return this._meridians
}
get points() {
    return this._points
}
get methods() {
    return this._methods
}
get symptoms() {
    return this._symptoms
}
get ratings() {
    return this._ratings
}
get selectedSymptom() {
    return this._selectedSymptom
}

get selectedRating() {
    return this._selectedRating
}

get selectedMethod() {
    return this._selectedMethod
}
get selectedMeridian() {
    return this._selectedMeridian
}
get selectedPoint() {
    return this._selectedPoint
}
}