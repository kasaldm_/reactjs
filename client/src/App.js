import { BrowserRouter } from 'react-router-dom';
import './App.css';
import AppRouter from './AppRouter';
import NavBar from './NavBar';
import Main from './pages/main';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';


const App = ()=> {
  
 
  return (
    <BrowserRouter >
      <NavBar/>
     <AppRouter /> 
    
    </BrowserRouter>

    
  );
};

export default App;
